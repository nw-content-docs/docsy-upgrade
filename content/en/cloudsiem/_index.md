---
title: NetWitness CloudSIEM
description: This topic provides information about CloudSIEM and its usage.
linkTitle: NetWitness CloudSIEM
---
## Introducing NetWitness Cloud SIEM

NetWitness® Cloud SIEM is a cloud-delivered log management, retention, and analytics solution that provides high-performance [SIEM](https://www.netwitness.com/en-us/products) (Security Information and Event Management) capabilities without the need for customer managed deployment and platform administration. 

NetWitness Cloud SIEM provides enterprises with the same rich log management, retention, reporting, and analytics services long utilized by NetWitness customers for threat detection and response, but in a cloud-delivered form. This new deployment option makes it easy for new or existing NetWitness customers to take advantage of Evolved SIEM without the resources associated with planning, sizing, deploying, updating, and administering the platform in a traditional data center or self-managed cloud format.

The [NetWitness Platform](https://www.netwitness.com/) is a leader in enterprise-grade threat detection and response.  Businesses and government agencies around the globe use NetWitness to address their demanding security requirements. Skilled threat hunters choose NetWitness as their go-to solution, due to its ability to rapidly analyze and process large volumes of information from many different sources.  

Security compliance teams have long depended on NetWitness to help meet stringent compliance needs by providing retention of large amounts of data while being able to provide fast, reliable access for compliance activities.

## How is NetWitness Cloud SIEM different then NetWitness Logs?

NetWitness Cloud SIEM provides the same software and technical capabilities as a customer-managed instance of NetWitness Logs. NetWitness Cloud SIEM is a SaaS, cloud-hosted offering of NetWitness Logs, running the latest version of NetWitness Platform software in Microsoft Azure.

NetWitness Cloud SIEM provides defined retention periods for log data (subject to data limits), based on the offering Tier selected by a Customer.
Cloud SIEM is also fully managed, administered, and updated by NetWitness, which is uniquely different then a customer-managed NetWitness Logs deployment.

As a result of Cloud SIEM being a cloud service there are certain administrative controls and limitations in place with the product to ensure availability and service level agreements for Customers. Specific limitations are outlined in this document and additional details are available upon request.

## Available Regions

NetWitness Cloud SIEM is generally available for data residency in the following Microsoft Azure regions:

- West US
- Germany West Central
- France Central
- UK West
- West India
- Southeast Asia
- Japan West
- Australia Central

Support for additional Azure regions may be added as new regions are made available.

## Log Data Collection

NetWitness Cloud SIEM enables seamless and secure methodologies for collecting log data from hundreds of event sources, extensively outlined in product documentation. Customers are responsible for configuring Cloud SIEM event sources after their tenant is provisioned. Many event sources are easily configured, including on-premise, virtual, hybrid, IaaS, PaaS, SaaS, and non-traditional sources, as outlined in documentation. 

NetWitness Professional Services is also available to assist Customers with event source onboarding on a paid basis. 

**Log Collection Deployment**

Log collection by NetWitness Cloud SIEM requires deployment of a Log Collector / Virtual Log Collector. When deploying a Log Collector, you must configure it to collect log events from various event sources and deliver these events both reliably and securely to the Cloud SIEM Log Decoder. Events are parsed and stored for subsequent analysis by the Log Decoder. Please note that each Cloud SIEM environment is self-contained within a single instance.

![Architecture](/images/products/cloudsiem/architecture.png)

**Details on Log Collection Methods**

Log Collector services are deployed across multiple locations to collect log data from varying sets of event sources by setting up an On Prem Log Collector which will collect the logs locally and then transfer them to the NetWitness Cloud Instance via an encrypted and compressed transport. All Current NetWitness Collection methods are supported in the NetWitness Cloud Instance.

For additional information regarding configuration and management of data sources, please refer to:
- The Log Collection configuration guide for additional details on Log and event source configuration.
- The Event Source Management guide for additional details on monitoring and managing event sources.
- The Integrations Catalog for a complete list of supported event sources.
- The Live Services Management Guide for configuring access to content enabling collection of the most current event source types.

**Data Compression**

The Communication between the on prem local collector and the NetWitness Cloud instance uses data compression to minimize the bandwidth required for data transmission to the NetWitness Cloud Instance.  In addition to data compression, the data is transmitted over an encrypted channel.

**OpenVPN Tunnel Configuration**

For secure communication between the on-premise Virtual Log Collector (VLC) and the cloud Log Decoder and NetWitness Server, you must configure a VPN tunnel. During the Cloud SIEM onboarding process, a download link will be provided with instructions on how to setup the communication link between the VLC and the Cloud SIEM infrastructure.

**Data Ingestion**

The NetWitness Cloud SIEM offering is licensed based on the amount of log data throughput ingested daily, measured in gigabytes (GB) per day of collected data. Data ingest monitors against the customers specified subscription entitlement (in GB/Day) and can be increased to a higher level of ingestion to account for any increase in the amount of data being collected. You can view current and past data ingestion information within the NetWitness UI licensing page. 

NetWitness Cloud SIEM customers may experience performance degradation and reduced data retention periods when log data throughput exceeds the contracted subscription capacity. NetWitness will be monitoring customers storage capacity and will inform them if additional ingestion or storage capacity is required. Customers are encouraged to contact NetWitness Sales to purchase an appropriate subscription plan to handle the required capacity.

## Network Connectivity and Data Transfer

All communication between customer-deployed Log Collectors/Virtual Log Collectors and the NetWitness cloud environment is encrypted. Connectivity to the NetWitness UI from the Customer location is limited by Azure specified firewall rules. Communication between various internal NetWitness services deployed in Azure are based on trusted certificates and are encrypted. To reduce bandwidth, data is compressed between various log collection sources and the NetWitness cloud environment.

Customers are responsible for all event source configuration, VLC setup, and initial connectivity to Azure cloud through Open VPN. For more information on data collection, please refer to the Log Data Collection section above. 

**Cloud SIEM Resiliency & Redundancy**

Compute / Virtual Machines:

Virtual Machines utilize a single instance per virtual machine while utilizing Premium Solid State Storage drives for all operating system disks. Additional details on Azure VM guidelines are available here: [azure.microsoft.com](https://azure.microsoft.com/en-us/support/legal/sla/virtual-machines/v1_9/) 

**Storage:**

Storage disks (OS and Data) utilize locally redundant storage (LRS) which replicates Customer data three times within the same Azure Availability Region. This prevents a failure from a failing SSD, failing server, and or failing network equipment.
-	[azure.microsoft.com](https://azure.microsoft.com/en-us/support/legal/sla/storage/v1_5/) 
-	[docs.microsoft.com](https://docs.microsoft.com/en-us/azure/storage/common/storage-redundancy) 

**Backups:**

The NetWitness Cloud SIEM creates configuration, OS, and VM backups (configuration data) of Customer hosts on a weekly basis which are retained for a period of two weeks. Prior to a NetWitness Cloud SIEM upgrade, two configuration backups are taken.

Backups within scope include:
- Configuration files for all Cloud SIEM services.
- Core databases and files for each Cloud SIEM service that reside on OS volumes and VM images.

Backups do not include any raw log data or metadata that is stored on independent solid-state drives (SSDs) which are not part of configuration, OS, or VM backups.

**Service Level Agreement**

NetWitness provides 24x7 infrastructure and product monitoring to meet an uptime SLA for the NetWitness Cloud SIEM offering. The NetWitness Cloud SIEM offering environment is considered available when users can: 
- Access the NetWitness UI
- Run queries & generate reports
- Ingest data from appropriately configured event sources
- Investigate threats
- Respond to incidents

## Maintenance and Administrative Services

The NetWitness Cloud SIEM service subscription provides routine management, maintenance, and updating to enable a high-performance experience with access to current features and functionality for all product users. There are maintenance and administrative activities also required by NetWitness Cloud SIEM product customers. NetWitness and customer-driven maintenance activities and restrictions are outlined in this section.

**NetWitness Maintenance And Administration Responsibilities**

- NetWitness application and services are operational.
- NetWitness core software services have minimal downtime.
- Timely releases of Upgrades, Patches and Hotfixes.
- No issues with VPN connectivity between NetWitness Cloud SIEM and log collectors, if used.
- Search and Reporting functionality work as expected.
- Alerts generation has minimal delay and Incidents are created for analysis.

**Maintenance And Upgrade Experience**

NetWitness Cloud SIEM is centrally maintained and upgraded by NetWitness to ensure that all customers are provided with a high-performance solution inclusive of the latest features and functionality. 

Each NetWitness Cloud SIEM instance will be upgraded to the previous minor release within 30 days of the most recent minor release. For example, NetWitness Cloud SIEM environments will be upgraded to v11.5.1 within 30 days of the release of NetWitness Platform v. 11.5.2. NetWitness Cloud SIEM end-user environments are purposefully maintained at the most recent previous minor release to ensure a high-quality experience and for any relevant bug-fixes to applied prior to upgrading. 

Routine maintenance and upgrade activities require planned downtime during which the NetWitness Cloud SIEM platform will be unavailable. End users will be notified of the planned service outage at least 14 days prior to the event, and will be provided with an option to elect a secondary window in the event of a potential conflict. NetWitness makes a best effort to schedule planned downtime for maintenance and upgrades outside of normal business hours to minimize operational disruption. 

If unscheduled emergency maintenance is required, NetWitness will make reasonable efforts to notify customer contacts of a downtime event. 

**Customer Maintenance And Administration Responsibilities**

- Log Collector or Virtual Log Collector is configured and can successfully communicate to NetWitness Cloud SIEM.
- Event Sources are configured properly for collection, including network connectivity. 
- If utilized, threat detection content is properly updated.
- Communication w/ NetWitness support related to planned downtime for maintenance activities.
- Updating primary points of contact with NetWitness Customer Support.
- Monitoring storage utilization to ensure adequacy for Customer’s desired retention periods.

**Shared Responsibilities**

As you consider and evaluate NetWitness Cloud SIEM services, it’s critical to understand the shared responsibility model and which deployment and administration tasks are handled by NetWitness and which tasks are handled by you.

![Shared Responsibilties](/images/products/cloudsiem/sharedresp.png)

**Customer Restrictions**

To meet the outlined Service Level Agreements for the NetWitness Cloud SIEM offering, the below Customer Restrictions are in place for the Cloud SIEM environment. These restrictions ensure the appropriate SLA is delivered to the Customer.
- No use of Linux dsadmin account.
- No use of NetWitness UI local admin account.
- No adjustment of Linux LVMs.
- No adjustment of settings in NetWitness Explore tab.
- No installation of additional Linux packages or repos on servers.
- No use of ElasticSearch admin account.
- No adjustment to OpenVPN Configurations.
- No data egress beyond allowances outlined in Performance Considerations section.

**Technical Support**

All NetWitness Cloud SIEM subscriptions include access to NetWitness enhanced technical support plan. Enhanced Support delivers 24 x 7 around-the-clock remote support and access to NetWitness global network of support centers for troubleshooting. 

Leverage our Secure Remote Support to ensure your issues are proactively managed in our Global Support Centers. 

Please visit [Support Plans and Options page](https://community.netwitness.com/t5/support-information/tkb-p/support-information) for more information.

**NetWitness Personalized Support Options**

NetWitness Personalized Support Options are designed to complement NetWitness service contracts with access to technical experts any time, day or night, and provide customers with a strategic personalized support relationship. With NetWitness Personalized Support Options, organizations can enjoy a support relationship with NetWitness that encompasses the entire product life cycle—from initial product integration to ongoing utilization. 

The following Personalized Support Options offer supplemental services that can be added to any new or existing NetWitness support contract:
- Service Account Manager (SAM): This option adds a dedicated Service Account Manager(s) to function as an internal advocate who works with primary contacts within a customer organization. The SAM option provides a designated contact responsible for providing weekly reporting, onsite account reviews, priority support, request for enhancement visibility, and more.
- Designated Support Engineer (DSE): This option provides specialized expertise with a technical contact personally accountable for ensuring the fastest possible remote resolution to questions and problems for product-specific issues. 

Please visit the Support Plans and Options page for additional information regarding NetWitness Technical Support and Personalized Support options. 

## Performance Considerations & Limitations

**Compute**

Each NetWitness Cloud SIEM instance is allocated dedicated cloud compute resources to enable effective utilization of the product. 
- Compute resource requirements are determined by rate of ingest corresponding to each Cloud SIEM subscription. 
- Certain user-driven behaviors such as utilization of the NetWitness Cloud SIEM product beyond the subscribed throughput license capacity may lead to performance degradation.
- Certain non-traditional use cases that require high-levels of compute and/or memory resourcing and fall outside the scope of the NetWitness Cloud SIEM product may lead to performance degradation.

If performance degradation is suspected, customers should engage NetWitness Customer Support to determine options for remediation.

**Storage**

The base subscription offering for NetWitness Cloud SIEM includes adequate high-performance solid-state storage to enable retention of 90 days of log data at the license limit purchased. In addition to the primary allocation of high-performance storage included with each NetWitness Cloud SIEM subscription, customers may purchase storage add-ons enabling extended retention periods.  

The NetWitness Cloud SIEM offering enables storage of log data to suit a variety of security and compliance use cases. 

Storage allocations are dedicated to each NetWitness instance/tenant. 
- Storage is allocated as a fixed quantity determined by the daily rate of ingest for a Customer based on their throughput subscription.
- Retention periods may vary based upon the actual rate of daily ingest, log sizing, log enrichment, or other practical variables.

Customers should monitor storage utilization to ensure achievement of their desired retention period. If retention periods are falling below Customers’ required retention periods they should contact NetWitness Customer Support to discuss options.

**Data Ingress/Egress**

There is no incremental cost for transporting data into the NetWitness Cloud SIEM offering, beyond the agreed subscription pricing. Each NetWitness Cloud SIEM instance is provisioned with a monthly data egress allowance of 2 TB/month to enable an effective product experience for most use-cases. 

Data egressed or exported from Customer’s Netwitness Cloud SIEM but remains within the same Azure Availability Zone is exempt from limitations and additional costs. 

Bulk data export is not included in the Customer’s data egress allowance. If additional data egress capacity is required for any circumstance, customers must engage a NetWitness Sales, Services, or Support representative to explore suitable options.

## Subscription Management & Options

**Subscription Options**

NetWitness Cloud SIEM is metered based on the amount of log data throughput ingested daily, measured in gigabytes (GB) per day. The throughput-based subscription measures data ingest against a subscription entitlement and can always be increased to a higher rate to account for any increase in the amount of data being collected. 

Customers may evaluate current and past daily data ingestion information on the licensing page within the NetWitness Cloud SIEM user interface.  
- Base Cloud SIEM Offering: Measured by GB/Day of data ingested (starting at a minimum of 50 GB/Day) including sufficient storage for 90 days of data retention

The Storage add-ons presently available across all supported regions are as follows: 
- 90-Day Hot Storage Add-On: The 90-Day Hot Storage Add-On extends the base 90-day retention period using high-performance premium solid-state storage. This yields a total retention period objective of 180 days. 
- 365-Day Warm Storage Add-On: The 365-Day Warm Storage Add-On provides 365 days of retention using spinning disk (HDD) storage. The first 90-days of this storage will overlap with the premium solid-state storage in the Base Offering. This Add-On utilizes the NetWitness Archiver capability to provide an economical solution for long-term log retention primary for compliance-driven use cases. This is not a suitable add-on for rapid queries or threat detection and investigation.

**Subscription Expansion**

Customers interested in growing their utilization of the NetWitness Cloud SIEM offering may pursue expansion of their relationship at any point during the subscription term. Expansions can coincide with the remainder of the subscription term, be applied for a limited duration, or for a single instance. Expansion of the NetWitness Cloud SIEM offering may be pursued via the purchase of:
- Additional daily ingest capacity
- Storage add-ons for extended retention
- Additional data egress capacity

**Subscription Renewals**

NetWitness Cloud SIEM customers will receive a renewal notification within 90 days of subscription expiration. 
- Customers are encouraged to pursue information regarding renewals at any point during their subscription term to confirm pricing, commercial requirements, or new releases of NetWitness product or service offerings. 
Confirmation of an intent to renew is recommended within 30 days of subscription expiration to ensure service continuity. 

Contact your NetWitness Account Manager at any point during your subscription period with questions regarding renewal policies or processes. 

**Subscription Termination**

On the date of Subscription Expiration, all functionality of the NetWitness Cloud SIEM product is suspended. The Cloud SIEM product will cease standard operational functionality including ingestion of log data upon subscription expiration. 

Contact your NetWitness Account Representative if access to the Cloud SIEM service is required beyond the date of expiration. 

After expiration, an image of the NetWitness Cloud SIEM product configuration and data will be retained for 30 days to enable reinstatement of the service. Log data will be retained for 30 days following expiration to support download or migration to another data-store as needed. 

- In the event of Subscription Termination and a requirement for Data Egress, data egress charges may apply for bulk data migration. 

The NetWitness Cloud SIEM dedicated instance, configurations, and log data are fully destroyed following 30 days of subscription expiration using industry standard best practices for data destruction.

