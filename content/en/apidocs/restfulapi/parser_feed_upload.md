---
title: Parser/Feed Upload
description: Parser/Feed Upload
weight: 140
---

## Parser/Feed Upload

You can upload Parsers and Feeds using the REST service.

{{% codeinfo %}} http://(hostname):(port)/decoder/parsers/upload {{% /codeinfo %}}  

If you point a browser to this URL, the web page lets you select a parser or feed file for upload.
You can also force a reload by selecting the toggle or providing the parameter {{% highlightinfo %}} reload=1 {{% /highlightinfo %}} on the URL.