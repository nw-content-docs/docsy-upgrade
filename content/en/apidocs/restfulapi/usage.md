---
title: USAGE
description: USAGE
weight: 110
---

## USAGE

The REST API accepts commands by using URL parameters and by POSTing application/json.
The special content type,  {{% highlightinfo %}} application/x-netwitness-string-params {{% /highlightinfo %}}, passes parameters, as plain text, in the format:`

{{% highlightinfo %}} param1=value1 param2="value \"2\"" {{% /highlightinfo %}}

{{% notice note %}}
Quotes, as part of the value, must be preceded by the backslash \ character. Any character can be escaped in this manner, including the backslash itself \\.
{{% /notice %}}

The format of the URL consists of the following components:

{{% highlightinfo %}} http://(postname or IP):(port)/[node1][/node2][…]?msg=<message name>[&param1=value1][&param2=value2][…] {{% /highlightinfo %}}

<br>

### Usage Example: the "/logs" Node

The /logs node supports several different messages:

* **Is** - Returns a list of child nodes. It supports the parameters depth and options.

* **mon** - Monitor this node (and possibly descendants) for changes. However, this message is not supported by the REST API because it requires a persistent connection and pipe that cannot be done via REST. Monitoring currently requires the full NetWitness SDK library.

* **pull** - This command pulls logs from the service. It supports two parameters: **count**, which controls how many logs to return, and **id2**, which controls the ending log ID to return. **id2** is optional and when it is not provided, the last log written is returned.

* **info** - Returns detailed node information. 

* **help** - The parameters are covered in more detail in [Find More Details](#find-more-details).

* **count** - A simple command to return the number of child nodes.

* **stopMon** - Stop monitoring the node from a previous mon command (also not supported by REST).

* **download** - A more complicated command to download a large number of log messages with several parameters to control log types and text matching capabilities. Like the **mon** command, this requires more than a simple request/response, which is not supported by the REST node interface.

* **timeroll** - Any log entries that exceed a given age are deleted.

To get a full list of NetWitness messages and parameters, use the help message:

{{% codeinfo %}}
http://localhost:50106/logs?msg=help&force-content-type=text/plain
{{% /codeinfo %}}

This command returns:
{{% highlightinfo %}}
<br>
description: A container node for other node types
<br> 
security.roles: everyone,logs.manage 
<br>
message.list: The list of supported messages for this node 
<br>
ls: [depth:(uint32)] [options:(string)] [exclude:(string)] 
<br>
mon: [depth:(uint32)]
<br>
pull: [id1:(uint64)] [id2:(uint64)] [count:(uint32)] [timeFormat:(string)]
<br> 
info: 
help: [msg:(string)] [op:(string)] [format:(string)]
<br> 
count:
<br> 
stopMon: 
<br>
download: [id1:(uint64)] [id2:(uint64)] [time1:(date-time)] [time2:(date-time)] op:(string) [logTypes:(string)] [match:(string)] [regex:(string)] [timeFormat:(string)] [batchSize:(uint32)] 
<br>
timeRoll: [timeCalc:(string)] [minutes:(uint32)] [hours:(uint32)] [days:(uint32)] [date:(string)] 
<br>
debugGen: [count:(uint32)]
<br>
{{% /highlightinfo %}}


### Example Syntax

To view the last 100 logs:
{{% codeinfo %}} 
http://hostname:50105/logs?msg=pull 
{{% /codeinfo %}}

To view the logs in XML format:
{{% codeinfo %}} 
http://hostname:50105/logs?msg=pull&force-content-type=text/xml 
{{% /codeinfo %}}

To see the last 10 logs in plain text:
{{% codeinfo %}} 
http://hostname:50105/logs?msg=pull&count=10&force-content-type=text/plain 
{{% /codeinfo %}}

### Find More Details

For more detailed information about a message (for example the pull message), request help specific to just that message. The **help** message displayed above uses the parameter name **msg**, but in the URL below, **message** is used, an alias for the help **msg** parameter to avoid conflicts with the REST API **msg**.

{{% codeinfo %}} 
http://localhost:50106/logs?msg=help&message=pull&force-content-type=text/plain 
{{% /codeinfo %}}

Returns this text:

{{% codeinfo %}} 
Pull: 

 Downloads N log entries security.roles: logs.manage parameters: id1 - (uint64, optional) The first log id number to retrieve, this is mutually exclusive with id2 id2 - (uint64, optional) The last log id number that will be sent, defaults to most recent log message when id1 or id2 is not sent count - (uint32, optional, {range:1 to 10000}) The number of logs to pull timeFormat - (string, optional, {enum-one:posix|simple}) The time format used in each log message, default is posix time (seconds since 1970) {{% /codeinfo %}}