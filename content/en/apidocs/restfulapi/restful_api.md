---
title: RESTful API
description: RESTful API
weight: 100
---

## RESTful API

The RESTful API that ships with NetWitness Platform XDR is a way to programmatically communicate with the NetWitness architecture. It must be enabled by setting {{% highlightinfo %}} /rest/config/enabled {{% /highlightinfo %}} to on, which is the default. The default port for communication is the default port + 100 (for example, on a Concentrator, the default port is 50005, and the REST API is on port 50105), but that can be changed by setting the {{% highlightinfo %}} /rest/config/port {{% /highlightinfo %}} parameter. SSL is controled by the setting in {{% highlightinfo %}} /sys/config/ssl {{% /highlightinfo %}}. For information about how to perform these tasks, see [Access the RESTful API in NetWitness Platform XDR](/nwcontent/restfulapi/access_the_restful_api_in_netwitness_platform_xdr/) in NetWitness Platform XDR.

The API is based on HTTP and is quite easy to use. The acceptable output formats are:

* text/plain
* text/xml
* text/html
* application/json

The content type that is returned can be controlled through the **HTTP Accept** header. It is possible to set the parameter **force-content-type** to one of the previous values.

The easiest way to begin is to point a browser to the REST port (for details about how to perform these tasks, see [Access the RESTful API in NetWitness Platform XDR](/nwcontent/restfulapi/access_the_restful_api_in_netwitness_platform_xdr/) in NetWitness Platform XDR. 

 PATH: http://(hostname or IP):(REST port) 

This command performs the default operation of  {{% highlightinfo %}} ls {{% /highlightinfo %}}, and returns a listing for the root node tree used by NetWitness:

<div>
 <img src="/images/products/restfulapi/classic-ui-11.3.1.png" alt="classic ui 11.3.1" width="890" height="min">
</div>

The user interface that is shown above is enabled by default in all Version 10 and 11 deployments. The user interface with the dark background (shown below) is available in Version 11.3.1 and later, and must be enabled if you want to use it.

<div>
     <img src="/images/products/restfulapi/ember-ui-opening-page.png" alt="ember ui opening page" width="890" height="min">
</div>

To enable the user interface with the dark background, set {{% highlightinfo %}} /rest/config/Enable WebSockets Interface (allow.websockets) {{% /highlightinfo %}} and restart the service. You can change the user interface back to the light background by setting this parameter to {{% highlightinfo %}} false {{% /highlightinfo %}} and restarting the service.


