---
title: Statistics Graph
description: Statistics Graph
weight: 150
---

## Statistics Graph

The REST interface has a built-in statistics graphing tool, which helps you monitor performance for a service during a specified time period. You can use this tool to collect and graph single or multiple statistics from a host during a specific time range. You can also graph real-time statistics.

To access the statistical graphing tool:

1.  From the root node tree page, click {{% highlightinfo %}} sdk {{% /highlightinfo %}}. (For information on accessing the root tree note page, see [Access the RESTful API in NetWitness Platform XDR](/nwcontent/restfulapi/access_the_restful_api_in_netwitness_platform_xdr/) in NetWitness Platform XDR.

    <div>
       <img src="/images/products/restfulapi/commandDefault-sdkSelected.png" alt="command Default sdk Selected" width="890" height="min">
    </div>

    <br>

    The options for additional functionality are displayed:

    <div>
       <img src="/images/products/restfulapi/AdditionalFunctionality.png" alt="Additional Functionality" width="890" height="min">
    </div>

    <br>


2.  Click {{% highlightinfo %}} sdk/app/stats {{% /highlightinfo %}}. The Performance Monitor window opens, and statistical graphs are displayed at the bottom of the page.

    <div>
       <img src="/images/products/restfulapi/statsGraphs.png" alt="stats Graphs" width="890" height="min">
    </div>

    <br>

Click the Help buttons for detailed information about this page and the fields it contains.



