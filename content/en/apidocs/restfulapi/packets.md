---
title: Packets
description: Packets
weight: 130
---

## Packets

You can retrieve a **pcap** file using the REST service.

{{% codeinfo %}} http://(hostname):(port)/sdk/packets {{% /codeinfo %}} 

If you point a browser to this URL, the web page lets you enter a list of session IDs or a time range. When you click **Submit**, it generates a **pcap** based on the supplied criteria.

Programmatically, using HTTP GET or POST, submit either a **sessions** parameter with a comma-separated list of session IDs and session ranges (#‐#) or a **time1** and **time2** parameter. Times must be in the format **YYYY-MM-DD HH:MM:SS**, for example: **2010-Apr-20 09:00:00**.

{{% notice note %}}
Since the list of sessions can get quite long, this API accepts the content-type {{% highlightinfo %}} application/x-netwitness-string-params {{% /highlightinfo %}}  for a **POST** command.
{{% /notice %}}

### Importing Packets

You can import packets to a DECODER using the REST service.

{{% codeinfo %}}
http://(hostname):(port)/decoder/import </code>
{{% /codeinfo %}}


If you point a browser to this URL, the web page lets you select a **pcap** file for upload. It also accepts pcap files POSTed to this URL.

REST begins processing incoming data immediately after the HTTP header is parsed. This means import of a **pcap** file occurs quickly and allows for large transfers. There is still a limit, but it is much larger (GBs) and is based on how well the import process can keep up with the client POST coming in. If the client posts a huge pcap (many GBs), it is still possible to overfill the current buffer. Any pcap that is 4 GBs or less should be able to process without issue.

{{% notice note %}}
The DECODER cannot be concurrently importing or capturing, or an error results.
{{% /notice %}}

