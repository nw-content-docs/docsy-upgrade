---
title: Access the RESTful API in NetWitness Platform XDR
description: Access the RESTful API in NetWitness Platform XDR
weight: 120
---

## Access the RESTful API in NetWitness Platform XDR

This topic describes how to enable the REST API in NetWitness Platform XDR. The REST API must be enabled by setting {{% highlightinfo %}} /rest/config/enabled {{% /highlightinfo %}} to on, which is the default. The default port for communication is the default port + 100 (for example, 50105 for a Concentrator), but that can be changed by setting the {{% highlightinfo %}} /rest/config/port parameter {{% /highlightinfo %}} . SSL is controled by the setting in {{% highlightinfo %}} /sys/config/ssl {{% /highlightinfo %}}.

{{% notice note %}}
By default, the REST interface accepts **BOTH** SSL and Non-SSL connections on the REST port. By setting {{% highlightinfo %}} /sys/config/ssl {{% /highlightinfo %}} to on, **ONLY** SSL connections will be accepted on the REST port.
{{% /notice %}}

To enable the REST port: 

1. In the NetWitness web user interface, go to <img src="/images/products/restfulapi/adminicon.png" class="inline"/> (**Admin**) > **Services** and select a service, for example, a Concentrator.

2. In the **Host** column, click the host name. The Hosts page opens, and the IP address of the host is displayed in the **Host** column. Make a note of the IP address. 

   {{% notice note %}}
    If the IP address listed in the Host column is the same as the IP address of the NetWitness web UI, the API is not available for that service.
   {{% /notice %}} 

3. Go to <img src="/images/products/restfulapi/adminicon.png" class="inline"/> (**Admin**) > **Services**, select the service, and then select **View** > **Config**. Under System **Configuration**, note the port number. You will use this port number as a basis for accessing the API, but you must add 100 to it. For example, if the port number is listed as **50005**, you would enter 50105.

4. In the browser, type the IP address of the service and append the port number to the IP address as shown here:
{{% highlightinfo %}} http://(hostname or IP address):(port) {{% /highlightinfo %}} 

    {{% notice note %}}
     The URL is HTTP, and not HTTPS.
    {{% /notice %}} 

5. In the Authentication dialog, enter the user name and password and click **Log in**. The root node tree used by NetWitness is displayed:

   <div>
     <img src="/images/products/restfulapi/commandDefault.png" alt="command Default" width="890" height="min">
   </div>

<br>  

### (Optional) Configure Custom SSL Certificate for the REST Interface

 {{% notice important %}}
 As a Security best practice, using self-signed certificates is not recommended.
 {{% /notice %}} 

{{% notice note %}}
In version 11.6, custom certificates are not supported on Log Collector.
{{% /notice %}}

By default, NetWitness Platform XDR provides certificates for all the NetWitness Platform XDR Core Services. For example, Decoder uses the {{% highlightinfo %}} /etc/netwitness/ng/decoder_cert.pem {{% /highlightinfo %}} certificate.

You can provide your own custom certificate (preferably issued by a certification authority) for the REST ports. The custom certificates must be in the OpenSSL PEM format. Perform the following steps to configure the custom certificates:

* Rename the custom certificate to {{% highlightinfo %}} _rest_key.pem {{% /highlightinfo %}} and upload it to {{% highlightinfo %}} /etc/netwitness/ng {{% /highlightinfo %}}. 

For example, {{% highlightinfo %}} decoder_rest_cert.pem {{% /highlightinfo %}}.

* Rename the private key to {{% highlightinfo %}} _rest_key.pem {{% /highlightinfo %}} and upload it to {{% highlightinfo %}} /etc/netwitness/ng {{% /highlightinfo %}}.

For example, {{% highlightinfo %}} decoder_rest_key.pem {{% /highlightinfo %}}.

{{% notice note %}}
You can combine the certificate and the private key in a single file and name it as {{% highlightinfo %}} (service)_rest_cert.pem {{% /highlightinfo %}}. For example, {{% highlightinfo %}} decoder_rest_cert.pem {{% /highlightinfo %}}.
{{% /notice %}}

If custom certificates are not detected, the default NetWitness Platform XDR certificates are used. 





