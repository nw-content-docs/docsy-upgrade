---
title: SDK Commands
description: SDK Commands
weight: 160
---

## SDK Commands

All queries on the system are performed by commands sent to the **/sdk** node.

The **/sdk** node has built-in help documentation for each message. To view the help for each command, click on the asterisk (*) beside the sdk node and then choose one of the messages from the drop-down menu. The documentation for the message is displayed in the Output window at the bottom of the screen.

To access the help:

1. From the root node tree page, click sdk. For information on accessing the root tree note page, see [Access the RESTful API in NetWitness Platform XDR](/nwcontent/restfulapi/access_the_restful_api_in_netwitness_platform_xdr/) in NetWitness Platform XDR.

2.  Click the asterisk (*) next to sdk.

    <div>
       <img src="/images/products/restfulapi/commandDefault-sdkSelected.png" alt="command Default sdk Selected" width="890" height="min">
    </div>
    <br>

    <div>
       <img src="/images/products/restfulapi/sdkhelp1.png" alt="sdk help 1" width="890" height="min">
    </div>

   <br>
      Information about the /sdk node is displayed.

<br>


3. To find more specific information, select a property from the **Properties for /sdk** drop-down menu:

    <div>
       <img src="/images/products/restfulapi/sdk-help-properties-menu.png" alt="sdk help properties menu" width="890" height="min">
    </div>

   <br>
      The help for the property that you selected is displayed in the Output section:


   <br>

### SDK Commands Further Reference

This guide should be used in conjunction with the SDK documentation, which explains the format of queries and results. This document primarily focuses on how to send queries and parameters via the REST API, not how the queries themselves are formatted. The Core Database Tuning Guide explains those concepts in detail.

 {{% notice note %}}
This document refers to several additional documents available on NetWitness Community Link. Go to the [NetWitness All Versions Documents](https://community.netwitness.com/t5/netwitness-platform-online/netwitness-platform-all-documents/ta-p/676246) and find NetWitness Platform XDR guides to the trouble shoot issues. All metadata returned via REST is encoded as **UTF-8**.
 {{% /notice %}} 

There is another parameter specific to the REST API called **expiry**. This parameter can be set to the number of seconds to wait for a response before the system returns a timeout error. The default is 30 seconds, which is sufficient for most requests. For queries, the standard SDK sets an infinite timeout. If you set **expiry** to zero (&expiry=0), this removes the timeout for that request. It is probably a good idea to set a larger timeout for queries and other requests that may take longer than 30 seconds during normal operations.



