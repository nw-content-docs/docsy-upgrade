---
title: Planning Considerations for Installing Cloud Link Service
description: This topic provides information about system requirements and various prerequisites.
weight: 80
---

## Planning Considerations for Installing Cloud Link Service

Before you install the Cloud Link Service, you must plan for the following:
+ The NetWitness Platform XDR (Log Decoder Host) is on version 11.5.2 or later.
+ Ensure you have at least 8 GB of memory on your host.
+ Ensure that the system clock is accurate. To fix the system clock, configure the NTP server on the Admin server. For more information on how to configure NTP server, see [Configure NTP Servers](https://community.netwitness.com/t5/netwitness-platform-online/configure-ntp-servers/ta-p/669729).
+ Ensure you have the administrator access to the NetWitness Platform on the Cloud user interface.
+ If you have an existing on-premises UEBA host deployed in your environment and you plan to move to Detect AI, you need to remove the host from the Admin server and stop the airflow-scheduler service on the UEBA host. If you plan to run UEBA and Detect AI simultaneously, see [Install Detect AI with an existing on-premises UEBA]({{< ref "/detectai/install-and-setup/uebadetectai" >}}).
+ The host on which the Cloud Link Service will be installed needs to be connected to Amazon Web Services(AWS). This might require changes to your existing firewall rules. Hosts will need to connect to the IP ranges for the chosen deployment region. For more information on the current list of AWS IPs by region, see [AWS IP address ranges](https://docs.aws.amazon.com/general/latest/gr/aws-ip-ranges.html).
+ Open TCP port 443 to allow outbound network traffic.
+ Ensure you have configured the Azure Monitor plugin in your deployment. This enables Detect AI to run a query for Azure AD log events for monitoring purposes in the correct format. For more information on how to configure the Azure Monitor plugin, see the *Azure Monitor Event Source Configuration Guide*.
+ (Optional) Ensure that you configure the proxy settings from NetWitness Platform version 11.5.3 or later, before installing the Cloud link Service. For more information, see [Configure the proxy for the Cloud Link Service]({{< ref "/detectai/install-and-setup/proxy" >}}).

To understand the deployment of the Cloud Link Service, see [Cloud Link Service Architecture]({{< ref "/detectai/install-and-setup/overview_cloudlinkservice#cloud-link-service-architecture" >}}).

{{% notice note %}}
Data will be fetched from only the host (Example: Log Decoder) on which the Cloud Link Service is installed.
{{% /notice %}}

You can install Cloud Link Service on the following hosts:

{{% table table-bordered %}}

| Model | Category |
| :--- | :--- |
| S5/S6/S6E/Virtual <br> Cloud (AWS, Azure, GCP) | Log Hybrid <br> Log Decoder <br> Endpoint Log Hybrid <br> Log Hybrid Retention <br> Virtual Log Decoder <br> Virtual Log Hybrid |
