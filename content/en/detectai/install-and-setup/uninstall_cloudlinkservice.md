---
title: Delete Cloud Link Service
description: This topic provides information about uninstalling the Cloud Link Service.
weight: 140
---

## Delete Cloud Link Service

If you have Cloud Link Service installed and no longer want to use it, perform the following steps to delete the Cloud Link Service.

{{% notice note %}}
When you delete the Cloud Link service, any data which are yet to be uploaded to the Detect AI will be discarded.
{{% /notice %}}

To delete the Cloud Link Service, first remove the Cloud Link Service from NetWitness Platform on the Cloud, and then uninstall the Cloud Link Service on the NetWitness Platform.

**Step 1: Remove the Cloud Link Service from the NetWitness Platform on the Cloud**
1. Log in to the NetWitness Platform on the Cloud.

2. Go to <img src="/images/products/detectai/icons/admin3.png" alt="Admin icon" class="inline"/> **Admin** > **Sensors** > **Sensor List**.

3. Select the Cloud Link Service that you want to delete, and click **Remove Sensor**.

**Step 2: Uninstall the Cloud Link Service on the NetWitness Platform**

1. SSH to the host on which the Cloud Link Service is installed.

2. Execute the following command:

   <div class="highlight">
     <pre class="chroma" style="padding-top:1rem; border: solid; border-width: thin; background-color: #F8F9FA";>
       <code>/var/lib/netwitness/cloud-link-server/nwtools/uninstall-cloud-link.sh</code>
     </pre>
   </div>
``

3. Log in to the NetWitness Platform XDR and go to <img src="/images/products/detectai/icons/adminicon.png" alt="Admin icon" class="inline"/> **Admin** > **Services** to verify if the Cloud Link Service is removed.