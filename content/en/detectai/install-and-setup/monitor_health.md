---
title: Monitor the Health of the Cloud Link Service
description: This topic provides information about how to access the service dashboard and monitor the health of the service.
weight: 100
---

## Monitor the Health of the Cloud Link Service

NetWitness Platform enables you to visualize the health of the Cloud Link Service similar to other NetWitness Platform services deployed in your environment. It helps you troubleshoot the problematic spikes, identify high resource usage, and gives a deep visibility into the source of problems before the service goes down.

Monitoring the health of the Cloud Link Service at all times enables you to keep track of the following parameters:

+ Status of all the Cloud Link Services in your deployment (offline and online).
+ For each Cloud Link Service, the sessions aggregation rate, sessions behind, and sessions collected.
+ Status of the uploads such as the count of sessions uploaded, the rate at which upload took place, and outstanding sessions to be uploaded.
+ CPU and memory usage of each service. 

**Prerequisites**

+ You must install the New Health and Wellness. For more information, see [New Health and Wellness](https://community.netwitness.com/t5/netwitness-platform-online/deployment-optional-setup-procedures/ta-p/668995#Health_Wellness)
+ You must ensure to download the Cloud Link Service dashboard from RSA Live and monitor the data transfer. For more information, see [Advanced Configurations](https://community.netwitness.com/t5/netwitness-platform-online/advanced-configurations/ta-p/669788)

The Cloud Link Service Dashboard provides key metrics as described in [Cloud Link Overview dashboard visualizations]({{< ref "/detectai/install-and-setup/dashboard_cloudlinkoverview" >}}).

**To access the Cloud Link Overview Dashboard**

1. Log in to the NetWitness Platform XDR.
2. Go to <img src="/images/products/detectai/icons/adminicon.png" class="inline"/> **Admin** > **Health & Wellness**.
3. Click **New Health & Wellness**.
4. Click **Pivot to Dashboard**.
   
   The Deployment Health Overview dashboard is displayed.

{{% notice note %}}
To view dashboards, your browser must be configured to allow popups and redirects.
{{% /notice %}}

5. Click <img src="/images/products/detectai/icons/horizontal_lines.png" alt="Dash icon" class="inline"/> and then click **Dashboard**.
   
   The Dashboards dialog is displayed.
6. Select the Cloud Link Overview Dashboard.

   You can look at the visualizations (charts, tables, and so on) to view current CPU and memory of Cloud Link Service, Sessions behind and Upload rate per Cloud Link Service, and so on.
7. You can adjust the time range on the top right corner and also use the host filter to view the visualizations on each host.

![aggregate](/images/products/detectai/aggregate_rate.png)
![Cloud Link Service Dashboard](/images/products/detectai/difference_rate.png)
![Cloud Link Service Dashboard](/images/products/detectai/memory_usage.png)
![Cloud Link Service Dashboard](/images/products/detectai/status.png)

<!-- **To access the Cloud Link Service monitor:** -->
<!-- 1. Log in to the NetWitness Platform. -->
<!-- 2. Go to <img src="AdminIcon.png" alt="Admin icon" class="inline"/> Admin > Health & Wellness. -->
<!-- 3. Click New Health & Wellness. -->
<!-- 4. Click Pivot to Dashboard. -->

   <!-- The Deployment Health Overview dashboard is displayed. -->

<!-- **Note:** To view dashboards, your browser must be configured to allow popups and redirects. -->

<!-- 5. Click <img src="monitor_1.png" alt="monitor icon" class="inline"/>, displays the alerts Dashboard view with available alerts. -->
<!-- 6. Click Monitor tab to view all the available monitors. -->

<!-- ![Cloud Link Service Dashboard](monitor.png) -->
