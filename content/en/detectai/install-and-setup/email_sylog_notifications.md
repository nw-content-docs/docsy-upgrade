---
title: Configure Email or Syslog Notifications to Monitor the Service
description: This topic provides information about configuring email or syslog notifications to monitor the service.
weight: 110
---

## Configure Email or Syslog Notifications to Monitor the Service

Notifications such as email or syslog can be configured to monitor the Cloud Link Service. You will be notified when the following events occur:

+ Cloud Link Service goes offline.
+ Offline Cloud Link Service is back online.
+ Cloud Link Service CPU, memory, or disk storage thresholds are exceeded.

{{% notice note %}}
You must install the New Health and Wellness to add the required notification. For more information, see [New Health and Wellness](https://community.netwitness.com/t5/netwitness-platform-online/deployment-optional-setup-procedures/ta-p/668995#Health_Wellness).
{{% /notice %}}

Notifications can be set up on the NetWitness Platform user interface by configuring the output, server settings, and notification. This is the notification type, namely email and syslog. When you set up a notification, you must specify the notification output for an alert.

### Configure email or syslog as a notification 

1. Go to <img src="/images/products/detectai/icons/adminicon.png" alt="admin icon" class="inline"/> **Admin** > **System**.
2. In the options panel, select **Global Notifications**.

   The Notifications configuration panel is displayed with the **Output** tab open.
   ![email notification](/images/products/detectai/global_notification.png)
3. On the **Output** tab, from the drop-down menu, select **Email** or **Syslog**.
   
   The following is an example of email notification:
   ![Define Email Notification](/images/products/detectai/email_notify.png)
4. In the Define Email Notification dialog, provide the required information and click **Save**.

### Configure email or syslog settings as a notification server
This is the source of the notifications and must be configured to specify the email server or syslog server settings.

1. Go to <img src="/images/products/detectai/icons/adminicon.png" class="inline"/> **Admin** > **System**.
2. In the options panel, select **Global Notifications**.

   The Notifications configuration panel is displayed with the **Output** tab open.

3. Click the **Servers** tab.
   ![Server settings](/images/products/detectai/server.png)
4. From the drop-down menu, select **Email** or **Syslog**.

   The following is an example for email server:
   ![Server](/images/products/detectai/servernotify.png)

5. In the **Define Email Notification Server** dialog, provide the required information and click **Save**.

### Add a email or syslog notification

1. Go to <img src="/images/products/detectai/icons/adminicon.png" alt="admin icon" class="inline"/> **Admin** > **Health & Wellness**.
2. Click **New Health & Wellness**.
3. Click **View Notifications Settings**.
4. Specify the following:
   
   + **Output Type**: Select the Notification type as Email or Syslog.
   + **Recipient**: Select the recipient based on the output type selected.
   + **Notification Server**: Select the notification server that will send the notification.
   + **Template**: Notification template as Email or Syslog.

5. If you want to add another notification, click **Add Condition** and repeat step 4.
{{% notice note %}}
You can specify a maximum of four conditions in the notification settings.
{{% /notice %}}
6. Click **Save**.