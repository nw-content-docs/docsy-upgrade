---
title: Cloud Link Service Installation
description: This topic provides information about installing Cloud Link Service and registering as a sensor.
weight: 90
---
## Cloud Link Service Installation

The administrators can perform the following tasks to install the Cloud Link Service successfully:

1. [Install Cloud Link Service](#install-cloud-link-service) 
2. [Download the Activation Package](#download-the-activation-package) 
3. [Register the Cloud Link Service](#register-the-cloud-link-service) 
4. [Verify if the Cloud Link Service is working](#verify-if-the-cloud-link-service-is-working) 
5. [Transfer Detect AI data to NetWitness Platform](#transfer-detect-ai-data-to-netwitness-platform) 


### Install the Cloud Link Service

You can install the Cloud Link Service on the following host types:
+ Log Decoder
+ Log Hybrid
+ Endpoint Log Hybrid
+ Log Hybrid Retention

**Prerequisites**

Ensure that the NetWitness Platform XDR and the host (Decoder) are on version 11.5.2.0 or later.

{{% notice note %}}
Data will be fetched from only the host (For Example: Log Decoder) on which the Cloud Link Service is installed.
{{% /notice %}}

**To install the Cloud Link Service**

1. Log in to the NetWitness Platform XDR as an administrator and go to <img src="/images/products/detectai/icons/adminicon.png" alt="admin icon" class="inline"/> **Admin** > **Hosts**.

   The Hosts view is displayed.

2. Select a host (Example: Log Decoder) and click <img src="/images/products/detectai/icons/installhst1.PNG" alt="install button" class="inline"/>.

   The Install Services dialog is displayed.

3. Select the **Cloud Link Service** from the Category drop-down menu, and click **Install**.
   ![Cloud Link Service Installation](/images/products/detectai/installcls.png)

4. Log in to the NetWitness Platform XDR, and go to <img src="/images/products/detectai/icons/adminicon.png" alt="download icon" class="inline"/> **Admin** > **Services** to verify successful Cloud Link Service installation.

### Download the Activation Package

You need the activation package to register Cloud Link Service with the NetWitness Platform on the cloud. The activation package can be used on all hosts containing Cloud Link Service, which you want to register and you can download it from the NetWitness Platform on the cloud.


**To download the activation package**

1. Log in to the NetWitness Platform on the Cloud.
2. Go to <img src="/images/products/detectai/icons/admin3.png" alt="admin icon" class="inline"/> **Admin** > **Sensors** > **Sensor Configuration**.
3. Under Download Activation Package, click <img src="/images/products/detectai/icons/generate_activation_package.png" alt="generate icon" class="inline"/> to generate the activation package.
4. Click <img src="/images/products/detectai/icons/download_activation_package2.png" alt="download icon" class="inline"/> to download the activation package.

### Register the Cloud Link Service

Registration of Cloud Link Service requires copying the activation package to the Cloud Link Service directory, and setting up the required permissions. Once this is completed, the Cloud Link Service will be registered automatically.

{{% notice note %}}
+ The same activation package can be used for multiple registrations.
+ Ensure you use the most recently downloaded activation package.
{{% /notice %}}

**Prerequisites**

Ensure that the system clock is accurate. To fix the system clock, configure the NTP server on Admin server. For more information on how to configure NTP Sever, see [Configure NTP Servers](https://community.netwitness.com/t5/netwitness-platform-online/configure-ntp-servers/ta-p/669729).

**To register the Cloud Link Service**

1. SSH to the host on which the Cloud Link Service is installed.
2. Copy the `device-activation-package.json` file downloaded from the NetWitness Platform on the cloud to the `/root` or `/temp` directory on the Cloud Link Service host.
3. Change the user and group of the `device-activation-package.json` file to `netwitness` by executing the following command:
   
   <div class="highlight">
      <pre class="chroma" style="padding-top:1rem; border: solid; border-width: thin; background-color: #F8F9FA";>
         <code>chown netwitness:netwitness device-activation-package.json</code>
      </pre>
   </div>

{{% notice important %}}
Avoid using <span style="font-weight: bold"> cp </span> command to add files under <span style="font-weight: bold"> /var/lib/netwitness/cloud-link-server </span> directory. The  <span style="font-weight: bold"> cp </span> command changes the user and group to <span style="font-weight: bold"> root </span>, which can result in the Cloud Link Service registration failure.
{{% /notice %}}

4. Move the `device-activation-package.json` file to the Cloud Link Service directory by executing the following command:

   <div class="highlight">
      <pre class="chroma" style="padding-top:1rem; border: solid; border-width: thin; background-color: #F8F9FA";>
         <code>mv device-activation-package.json /var/lib/netwitness/cloud-link-server/</code>
      </pre>
   </div>

5. To verify if Cloud Link Service is registered successfully, log in to the NetWitness Platform on the cloud, and check the status of the Cloud Link Service. For more information, see [Verify if the Cloud Link Service is working](#verify-if-the-cloud-link-service-is-working).

{{% notice note %}}
If you want to re-register a Cloud Link Service with a different activation package, first remove the Cloud Link Service from the NetWitness Platform sensor list on the cloud, and then uninstall Cloud Link Service on the NetWitness Platform. For more information about deleting the Cloud Link Service, see [Delete Cloud Link Service]({{< ref "/detectai/install-and-setup/uninstall_cloudlinkservice" >}}).
{{% /notice %}}

### Verify if the Cloud Link Service is Working 

You can check the status of NetWitness Platform Sensor List on the cloud to verify the successful registration of Cloud Link Service. The status must reflect as Connected for the Cloud Link Service to start transferring data. You can use this status to monitor the Cloud Link Service and troubleshoot registration failures.

**To verify the status of the Cloud Link Service**

1. Log in to the NetWitness Platform on the Cloud.
2. Go to <img src="/images/products/detectai/icons/admin3.png" alt="admin icon" class="inline"/> **Admin** > **Sensors** > **Sensor List**. <br>
The following information is displayed for every Cloud Link Service registered in your deployment:

{{% table table-bordered %}}

Detail | Description |
|:--- | :--- |
| Hostname | The host on which the Cloud Link Service is installed. Example: Endpoint Log Hybrid.|
| Status | Status of the Cloud Link Service: <br> - **Registered**: The Cloud Link Service is registered successfully. <br> - **Connected**: The Cloud Link Service is connected and operating normally. <br> - **Disconnected**: The Cloud Link Service is not connected. <br> - **Connecting**: The Cloud Link Service starts connecting. |
| Sensor Version | The installed version of the sensor. Example: 11.7.0.|
| Device Type | Type of sensor that is installed and registered. Example: CLOUD_LINK.|
| Uptime | Displays the sensor's uptime and downtime.|

<!-- Whenever there is a change in the connectivity status of Cloud Link Service and NetWitness Platform on the cloud, it triggers an email to your registered email ID. If you receive an email stating that the Cloud Link Service is disconnected from the NetWitness Platform on the cloud:
+	Check your internet connectivity
+	Check if the Cloud Link Service is offline -->

<!-- To monitor the data transfer for the Cloud Link Service, you must install and download the following: -->

<!-- + You must install the New Health and Wellness on the NetWitness Platform version 11.5 or later. For more information, see [New Health and Wellness](https://community.rsa.com/docs/DOC-101877#Health_Wellness). -->
<!-- + You must ensure to download the Cloud Link Service dashboard from RSA Live and monitor the data transfer. For more information, see [Advanced Configurations](https://community.rsa.com/docs/DOC-114079). -->

<!-- For more information on monitoring the health of the Cloud Link Service, see [How to monitor the health of the Cloud Link Service](/admin/monitorhealthcloudlinkservice/). -->

### Transfer Detect AI data to NetWitness Platform 

If you want to view the Detect AI data on your NetWitness Platform user interface you must configure the data transfer from the cloud to the Admin server. Perform the following steps:
   
{{% notice important %}}
This step should be performed only once after you register the Cloud Link Service for the first time.
{{% /notice %}}

1. SSH to the Admin server.
2. Execute the following command:

   <!-- `nw-manage --enable-cba` -->

{{% copycode %}}
<code> 
nw-manage --enable-cba </code>
{{% /copycode %}}



