---
title: Cloud Link Service overview
description: Introduction to Cloud Link Service and planning considerations for installing Cloud Link Service. 
weight: 80
---

## Cloud Link Service Overview

NetWitness Cloud Link Service enables you to use the NetWitness Detect AI solution and its features by providing a secure transportation mechanism between existing NetWitness Platform hosts (Decoders) and the NetWitness Detect AI service. Example: to perform analytics on the NetWitness Detect AI, you must install and register the Cloud Link Service on at least one Log Decoder host.

Cloud Link service is a sensor that you must install and register on your on-premise host to:
+ Transfers metadata from the host (such as Decoders) in your on-premises deployment to the NetWitness Detect AI for analysis and investigation.
+ Transfer alerts generated in NetWitness Detect AI to your on-premises NetWitness Platform Respond server for incident management.

You can install Cloud Link Service on the following host types:
+ Log Decoder
+ Log Hybrid
+ Endpoint Log Hybrid
+ Log Hybrid Retention

{{% notice note %}}
+ Cloud Link Service and the hosts must be on version 11.5.2.0 or later.
+ You need a separate Cloud Link Service to be installed for each host.
+ To support endpoint-related queries, Cloud Link Service must be on version 11.7.1.0 or later.
{{% /notice %}}

## Cloud Link Service Architecture
This section provides information on how data is transferred using Cloud Link Service:

### Single Deployment: Data Transfer

 ![flow 1](/images/products/detectai/flow1.png)

1. Cloud Link Service fetches all the metadata from the host. For example: Log Decoder.
2. The Cloud Link Service filters metadata from the following data sources:
     + Active Directory
     + Authentication
     + File
     + Process
     + Registry
3. Cloud Link Service collects only matching metadata, compresses the matching metadata, and transfers it to NetWitness Detect AI through a secure channel.


{{% notice note %}}
Cloud Link Service ensures that no data is lost during temporary network issues or outages. If the outage lasts for more than 7 days, then the data older than 7 days will not be considered.
{{% /notice %}}

### Multiple Deployment: Data Transfer

 ![flow 2](/images/products/detectai/flow2.png)

### Data Transfer from NetWitness Detect AI

NetWitness Detect AI transfers the alerts generated to the on-premises NetWitness Platform Respond server which can be viewed on the user interface for incident management.

 ![flow 3](/images/products/detectai/flow3.png)
