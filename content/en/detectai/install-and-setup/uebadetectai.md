---
title: Install Detect AI with an Existing On-premises UEBA
description: This topic provides information about installing Detect AI and UEBA together in an enviornment.
weight: 158
---

## Install Detect AI with an Existing On-premises UEBA

If you have UEBA deployed on your on-premises NetWitness Platform, you can install Detect AI and can run them simultaneously. This is because they are independent of each other. However, the User Interface can be connected to only one source at a time. 

When you have both UEBA on-premises and Detect AI running simultaneously, it can impact the performance as both consume data from the NetWitness Platform. Detect AI receives data from the Cloud Link Service installed on Log Decoder hosts, and the on-premises UEBA receives the data from the Concentrator or Broker. 

{{% notice note %}}
This feature is supported from the 11.6.0.0 version or later.
{{% /notice %}}

**Install and Setup Detect AI**

1. Install the Cloud Link Service. For more information, see [Install Cloud Link Service]({{< ref "/detectai/install-and-setup/cloudlinkservice-install#install-cloud-link-service" >}}).

2. Download the Activation Package. For more information, see [Download the activation package]({{< ref "/detectai/install-and-setup/cloudlinkservice-install#download-the-activation-package" >}}).

3. Register the Cloud Link Service. For more information, see [Register the Cloud Link Service]({{< ref "/detectai/install-and-setup/cloudlinkservice-install#register-the-cloud-link-service" >}}).

4. Verify the Cloud Link Service is working. For more information, see [Verify if the Cloud Link Service is working]("/detectai/install-and-setup/cloudlinkservice-install#verify-if-the-cloud-link-service-is-working" >}]).

5. Enable Detect AI data transfer by running the following command:

    `nw-manage --enable-cba`
   
   This command connects the Detect AI to the on-premises Admin Server, and the data in the Users page is fetched from the Detect AI. For more information, see [Transfer Detect AI data to NetWitness platform]({{< ref "/detectai/install-and-setup/cloudlinkservice-install#transfer-detect-ai-data-to-netwitness-platform" >}}).

   {{% notice note %}}
   If you want to receive the data from on-premises UEBA, run the following command:

   `nw-manage --disable-cba`

   This command connects the UEBA to the Admin Server and the data in the Users page is fetched from the on-premises UEBA.
   {{% /notice %}}

6. Enable the Detect AI incident rules. For more information, see [Step 1. Configure Alert Sources to Display Alerts in the Respond View](https://community.netwitness.com/t5/netwitness-platform-online/step-1-configure-alert-sources-to-display-alerts-in-the-respond/ta-p/669570).


**Uninstall Detect AI**

1. Uninstall the Cloud Link Services from the Decoders. For more information, see [Delete Cloud Link Service]({{< ref "/detectai/install-and-setup/uninstall_cloudlinkservice" >}}).

2. Contact the NetWitness Customer Support to uninstall all the related tenants and entitlements.

   If you want to reconnect to the on-premises UEBA, run the following command:

   `nw-manage --disable-cba`

   This command connects the UEBA to the Admin Server and fetch the data in the Users page
   from the on-premises UEBA.

