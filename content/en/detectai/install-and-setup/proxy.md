---
title: Configure the Proxy for Cloud Link Service
description: This topic provides information about configuring proxy support for Cloud Link Service.
weight: 171
---

## Configure the Proxy for Cloud Link Service

If you are using a proxy network, you can configure the proxy for the Cloud Link Service under the NetWitness Platform, System > HTTP Proxy Settings page. This allows the Cloud Link Service to connect using a proxy and transfers data to the Detect AI.

**To configure proxy for Cloud Link Service**

1. Log in to the NetWitness Platform XDR.
   
2. Go to <img src="/images/products/detectai/icons/adminicon.png" alt="admin icon" class="inline"/> **Admin** > **System**.

3. In the options panel, select **HTTP Proxy Settings**. 
   
   The HTTP Proxy Settings panel is displayed.

   ![Configure Proxy](/images/products/detectai/http1.png)
  
4. Click the **Enable** checkbox.
   
   The fields where you configure the proxy settings are activated.

5. Type the hostname for the proxy server and the port used for communications on the proxy server.
   
6. (Optional) Type the username and password that serve as credentials to access the proxy server if authentication is required.

7. (Optional) Enable **Use NTLM Authentication** and type the NTLM domain name.
   
8. (Optional) Enable **Use SSL** if communications use Secure Socket Layer.
   
9.  To save and apply the configuration, click **Apply**.
  
    The proxy is immediately available for use for the Cloud Link Service.
