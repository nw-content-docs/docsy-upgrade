---
title: Change the Default Service for Investigation 
description: This topic provides information about changing prefered Broker Service ID for investigation.
weight: 170
---

## Change the Default Service for Investigation  

By default, if you have a Broker installed on an Admin Server, then the service ID of a Broker will be automatically updated in Cloud Link Service as default service for investigation on the NetWitness Platform user interface for Detect AI. However, if there are no Brokers installed on an Admin server, then any one of the service ID of a Broker installed on another node will be automatically updated in Cloud Link Service. If you want to set a specific service ID for a Broker, you can configure in the Explore view of the Cloud Link Service on the NetWitness Platform user interface.

![Architecture](architecture.png)

**To locate the service ID for a Broker**

1. Log in to the NetWitness Platform.

2. Go to <img src="/images/products/detectai/icons/adminicon.png" alt="admin icon" class="inline"/> **Admin** > **Services**.

3. In the **Services list**, search Broker in the **Filter** field.

4. Select a Broker, and click <img src="/images/products/detectai/icons/actions_button.png" alt="action button" class="inline"/> > **View** > **Explore**.
   
   The Explore view for the Broker is displayed.

    <img src="/images/products/detectai/exploreTest.png" alt="services view" width="900" height="min">

5. On the left panel, click **sys** > **stats**.
   
   The service ID is displayed on the right panel.
   
   <img src="/images/products/detectai/brokerid.png" alt="services view" width="900" height="min">


**To set the service ID for a Broker**

1. Log in to the NetWitness Platform.

2. Go to <img src="/images/products/detectai/icons/adminicon.png" alt="admin icon" class="inline"/> **Admin** > **Services**.

3. In the **Services list**, search Cloud Link Server in the **Filter** field.

4. Select the Cloud Link Server and click <img src="/images/products/detectai/icons/actions_button.png" alt="action button" class="inline"/> > **View** > **Explore**.
  
   The Explore view for the service is displayed.

   <img src="/images/products/detectai/explorebroker.png" alt="services view" width="900" height="min">

5. On the left panel, click **cloudlink/sync**.

6. Edit and enter the required service ID of a broker in the default-service-for-investigation parameter field.

   <img src="/images/products/detectai/brokeridinvestigate.png" alt="services view" width="900" height="min">
   