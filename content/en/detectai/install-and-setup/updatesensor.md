---
title: Update the Cloud Link Service Automatically
description: This topic provides information about Upgrading the Cloud Link Service to new versions.
weight: 151
---

## Update the Cloud Link Service Automatically

You can now easily keep all your Cloud Link Service up-to-date with the latest version. You can set up automatic updates or scheduled updates to save time and avoid manual tracking of the Cloud Link Service. 

You can set up update options on the Sensor Configuration tab:

+ Automatic update: Select to allow auto-update of sensors as and when a new version is available.
+ Schedule update: Select to schedule auto-update of the sensor for a specific day and time.

**Prerequisites**
  
+ The NetWitness Platform (host) is on version 11.6.1 or later.
+ Ensure that the Cloud Link Service is in a connected state in the UI to start the update.

{{% notice note %}}
- The Sensor Update button will be enabled only when there is a new version available.
- During the update process, the Cloud Link Service will get disconnected and data transfer to the cloud will be paused. If the update fails, the Cloud Link Service will revert to the last installed version.
- Cloud Link Service will begin updating automatically within 10 minutes if the automatic update option is enabled.
{{% /notice %}}

**To update the Cloud Link Service automatically**

1. Log in to the NetWitness Platform on the Cloud.

2. Go to <img src="/images/products/detectai/icons/admin3.png" alt="admin icon" class="inline"/> **Admin** > **Sensors** > **Sensor Configuration**.
   
3. Do one of the following:
   + To setup the Automatic update: select the option **Automatically update when a new version is available**.
   + To setup the Schedule update: Select the option **Automatically update on selected date and time and set the day and time** based on your preference. <br>
      + Specify the time in HH:MM in **AT (UTC)** field. For example, 07:03.

{{% notice note %}}
To change the sensor Update settings at any point, select the preferred update option and click **Save**.
{{% /notice %}}

## Update the Cloud Link Service Manually

You can update the Cloud Link Service manually on selected hosts.

{{% notice note %}}
You can update the Cloud Link Service individually on each host. You cannot update multiple Cloud Link Services.
{{% /notice %}}

**To update the Cloud Link Service manually**

1. Log in to the NetWitness Platform on the Cloud.
2. Go to <img src="/images/products/detectai/icons/admin3.png" alt="admin icon" class="inline"/> **Admin** > **Sensors** > **Sensor Configuration**.
3. Select the option **Manually update each sensor** and click **Save**.
4. Click the **Sensor List** tab.
   
5. Select the Cloud Link Service that needs to be updated and click **Update Sensor**. 
   
   A pop-up message is displayed to confirm the update.
6. Click **Update**.

{{% notice note %}}
If the update fails, the error for update failure is displayed, and you can troubleshoot the Cloud Link Service and resolve the issue. For more information, see [Troubleshoot the Cloud Link Service]({{< ref "/detectai/install-and-setup/troubleshoot" >}}).
{{% /notice %}}

## Limitations for sensor update

Limitations associated with this version of the sensor are included below:

1. When a new version of the sensor update is available, for example, 11.6.1, the Sensor Update button is enabled and ready to update the sensor. When you click Sensor Update, the sensor update starts. However, at the same time if a new rpm for the sensor update is uploaded, for example, 11.7, there are high chances the sensor update will not be overridden, causing the sensor to not be updated with the latest version. 

2. When a new version of sensor update is available, and you have configured for manual updates, the sensor update will not be triggered automatically. In this scenario, you need to update to the new version manually. However, if a new version of the sensor update is released after changing the setting to automatic, all sensor updates will be performed automatically from that moment.


