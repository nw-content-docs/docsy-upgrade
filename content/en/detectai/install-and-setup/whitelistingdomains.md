---
title:  List of Domains required to be Whitelisted for Detect AI
description: This topic provides information about Domains required to be whitelisted for Detect AI.
weight: 172
---

## List of Domains required to be Whitelisted for Detect AI

In case your organization uses a firewall to restrict network access to only specific websites or software, you need to whitelist the following domains to ensure that Cloud Link Service can communicate with AWS-related services and transfer the required metadata to Detect AI for analytics.

+ These Domains/URLs will be region-specific for the deployment. The region can be found in the device activation package from the **region** section.

  + sts.us-**(region)**.amazonaws.com
  + s3.us-**(region)**.amazonaws.com
  + kinesis.**(region)**.amazonaws.com
  + monitoring.us-**(region)**.amazonaws.com
  + ssm.us-**(region)**.amazonaws.com

+ Besides the common domains you need to whitelist specific domains based on your deployment and are provided in the **device activation package**. Following are the names of domains/URLs:
  + deviceApi
  + controlApi
  + iotApi
  + iotHost
  + uebaApiGatewayUrl
  + uebaCloudFrontUrl
  
In the following example, with this device activation package, the given deployment is in us-east-1 region, and the highlighted domains are the ones that must be whitelisted for this deployment.
<img src="/images/products/detectai/whitelist_domains.png" alt="Whistelisting domains" width="900" height="min">

The following table shows the list of domains/URLs that are whitelisted for the deployment in the above example:

{{% table table-bordered %}}

| SlNo | Domain URL |
| :--- | :--- |
| 1  | sts.us-east-1.amazonaws.com |
| 2  | s3.us-east-1.amazonaws.com |
| 3  | kinesis.us-east-1.amazonaws.com |
| 4  | monitoring.us-east-1.amazonaws.com |
| 5  | ssm.us-east-1.amazonaws.com |
| 6  | abc8hgbvbk4.execute-api.us-east-1.amazonaws.com |
| 7  | ghbcfjkbc.execute-api.us-east-1.amazonaws.com |
| 8  | h7vcvkvjbhbb78.credentials.iot.us-east-1.amazonaws.com |
| 9  | fhgodewbcimb-ats.iot.us-east-1.amazonaws.com |
| 10 | xhhvbbej52.execute-api.us-east-1.amazonaws.com |
| 11 | bgyiohftrchkfz.cloudfront.net |
