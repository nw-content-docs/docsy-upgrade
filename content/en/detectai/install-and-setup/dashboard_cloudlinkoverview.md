---
title: Cloud Link Overview Dashboard Visualizations
description: This topic provides information about Cloud Link Service Dashboard.
weight: 105
---

## Cloud Link Overview Dashboard Visualizations

This topic provides information on the Cloud Link Overview dashboard. The dashboard contains information on  Cloud Link Service key metrics such as the hosts the Cloud Link Service is running on, outstanding sessions to be uploaded, CPU, memory usage, and so on.
<!-- Monitor is used to trigger an alert when an unexpected event occurs, when the sessions behind for Cloud Link Service exceeds the set threshold. -->


{{% notice note %}}
The metrics listed below are the default values. You can customize the visualizations based on your requirement. For example, you can customize a visualization to view the CPU utilization for all the Cloud Link Service.
{{% /notice %}}

**Cloud Link Overview Dashboard**

{{% table table-bordered %}}

Visualization | Metrics | Objective | Description |
| :---          | :---    | :---      | :---        |
| Sessions Aggregation Rate Per CLS | Sessions aggregated rate by all Cloud Link Service. | Provides the sessions aggregated rate for all Cloud Link Service to take necessary actions when the session aggregation rate goes down. | Displays the sessions aggregation rate for all Cloud Link Service. |
| Sessions Behind Per CLS | Sessions behind by each Cloud Link Service. | Provides the sessions behind trend on each Cloud Link Service to take necessary actions when the session behind goes higher. | Displays the sessions behind trend for each Cloud Link Service. |
| Sessions Collected | Sessions collected by each Cloud Link Service. | Provides the sessions collected trend for each Cloud Link Service to take necessary actions when the session collection rate goes down. | Displays the sessions collected trend for each Cloud Link Service. |
| Sessions Uploaded | Sessions uploaded by each Cloud Link Service. | Provides the sessions uploaded trend for each Cloud Link Service to take necessary actions when the session uploaded rate goes down. | Displays the sessions uploaded trend for each Cloud Link Service. |
| Difference - Sessions Collected and Uploaded  | Difference in Sessions collected and sessions uploaded count by each Cloud Link Service. | Provides the difference between the sessions collected count and sessions uploaded count for each Cloud Link Service to take necessary actions when the session value goes higher. | Displays the difference between the sessions collection count and sessions uploaded count for each Cloud Link Service. |
| Upload Rate per CLS | - Host name <br> - Upload rate | Provides the rate at which the Cloud Link Service uploads the sessions to the Detect AI. | Displays the upload rate of sessions from each Cloud Link Service to Detect AI. |
| Outstanding Sessions to be uploaded to Cloud per CLS | - Host name <br> - Count of Outstanding Records | Provides the outstanding session trend to identify any high values and take necessary action. | Displays the total number of sessions that have not been uploaded to Detect AI per Cloud Link Service. |
| Cloud Link Service by CPU Percentage | - Host name <br> - CPU usage | Identifies the CPU usage by Cloud Link Service to detect high use and take necessary action. | Displays the CPU usage by Cloud Link Service. |
| Cloud Link Service by Resident Memory Usage | - Host name <br> - Resident memory usage | Identifies the resident memory usage by Cloud Link Service to detect high use and take necessary action. | Displays the resident memory usage by Cloud Link Service. |
| Cloud Link Service Status | - Service name <br> - Service Status <br> - Status time | Provides the status of Cloud Link Service. | Displays the status of Cloud Link Service. |
| Offline vs Total Cloud Link Services | - Service name <br> - Service Status | Identifies the number of offline services with the total number of Cloud Link services in your deployment. | Displays the total number of Cloud Link services and the number of services that are offline. |



<!-- **Cloud Link Service Monitor** -->

<!-- |Number | Monitor | -->
<!-- | :---          | :---    | -->
<!-- | 1 | **Cloud Link Service - Sessions Behind:** This monitor triggers an alert when the sessions behind for the Cloud Link Service exceeds the set threshold. -->
