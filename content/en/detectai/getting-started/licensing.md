---
title: About Detect AI licenses 
description: This topic provides information about NetWitness Detect AI licenses.
weight: 80
---

## Types of Detect AI licenses 

NetWitness Detect AI licenses are subscription based licenses. The license entitlement is based on the number of users with a default data storage of 500 GB data storage capacity.

{{% table table-bordered %}}

License Type |  Limitations
|:--- |:--- |
| NetWitness Detect AI Subscription. This is based on the number of active user accounts monitored in your environment. | Capacity limited to 500 GB per day of data storage.|
| NetWitness Detect AI additional Daily Capacity Subscription. |  50 GB per day increments.

<!-- **Note:** If you need additional storage beyond 500 GB, you must purchase the Additional Daily Capacity Subscription license. -->



