---
title: log in to your Account
description: This topic provides information about how to access your NetWitness Detect AI account.
weight: 70
---

## log in to your Account

To log in to NetWitness Platform on the Cloud:

1. Click on the URL provided in the NetWitness Platform on the cloud welcome email.
   The NetWitness Platform on the Cloud home page is displayed.
2.  Enter your registered email ID and the temporary password in the respective fields.
    As this is your first login, the page prompts you to reset your password.
3.  Enter the new password, and confirm the same.
    Review the password format rules and ensure that your new password conforms to the indicated format rules.
4. Click **Sign In**.


