---
title: Enable Multi-factor Authentication for your Account
description: This topic provides information about how to Enable Multi Factor Authentication for your account.
weight: 100
---

## Enable Multi-factor Authentication for your Account

NetWitness Platform on the cloud offers Multi-factor authentication (MFA), using which you can configure an additional layer of credentials to secure your identity and manage access. If you enable MFA, then the administrative user will be prompted to additional identifications at the time of log in, such as verification code sent to the mobile number or mobile authentication application.

**To Configure MFA**

1. Go to <img src="/images/products/detectai/icons/admin3.png" alt="admin icon" class="inline"/> **Admin** > **Account Settings** > **Multi-Factor Authentication**.
  The Multi-Factor Authentication page is displayed.
2. Select **ON**, **OFF** or **OPTIONAL** as per your requirement.

The following table provides information on the different MFA settings that NetWitness Platform on the cloud offers:

**Multi-Factor Authentication Settings**

{{% table table-bordered %}}

MFA Setting | Description |
|:--- | :--- |
| ON | Select **ON** to activate MFA. A secret code will be sent to the registered email account of the new administrators. Administrators can log in to their account, and choose between the secret code or an authentication mobile application as their preferred authentication method.|
| OFF | Select **OFF** to deactivate MFA. Administrators can log in to their account with their registered email ID and password. |
| OPTIONAL | Select **OPTIONAL** if you want to let the administrators decide if they want to activate or deactivate MFA for their accounts. |


