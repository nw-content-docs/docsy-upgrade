---
title: Access Detect AI
description: How to access detect AI
weight: 60
---

## Access Detect AI

In order to view a list of all users monitored by Detect AI, you need to have access to the NetWitness Platform User Interface.

**To access NetWitness Detect AI**

1. Log in to NetWitness Platform.
2. Go to **Users** > **Overview**.

   The user activity details are displayed. 
   ![Users view](/images/products/detectai/112_Ovtab.png)
