---
title: Welcome to NetWitness Detect AI
description: This topic provides information about NetWitness Detect AI.
weight: 45
---

## Welcome to NetWitness Detect AI

NetWitness Detect AI is an add-on to NetWitness® Platform. The product is a SaaS service that analyzes NetWitness Platform data and triggers alerts on potential threats and malicious activity. NetWitness Detect AI takes all the traditional functions of NetWitness User and Entity Behavior Analytics (UEBA) and provides them as a native SaaS application. As a cloud service, Detect AI has many additional benefits including operations from the Operations team who manage the service for your organization which enables NetWitness to release new content and enhancements faster so security teams are better equipped to respond to threats.

NetWitness Detect AI is an advanced analytics and machine learning solution that leverages unsupervised machine learning and empowers Security Operations Center (SOC) teams to discover, investigate, and monitor risky behaviors in their environment. All users in an organization can be analyzed for abnormal user activities using log, and endpoint data already collected by your NetWitness Platform.

For existing NetWitness Platform customers, NetWitness Detect AI enables analysts to:

* Detect malicious and rogue users
* Pinpoint high-risk behaviors
* Discover attacks
* Investigate emerging security threats
* Identify potential attacker's activity

NetWitness Detect AI resides on an Amazon Virtual Private Cloud (VPC) and each organization has its own VPC. If you have an on-premises NetWitness Platform deployment in your network, metadata will be uploaded to the cloud for analysis.

NetWitness Detect AI performs advanced analytics on the data to enable analysts to discover potentials threats. Analysts will begin to see alerts and behavior profiles of users directly in their existing NetWitness Platform UI, and will be able to perform basic administration of the SaaS components from a dedicated SaaS UI.
![Detect AI Workflow](/images/products/detectai/ueba_2.png)