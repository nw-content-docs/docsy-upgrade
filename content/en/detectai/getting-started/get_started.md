---
title: Getting Started with Detect AI
weight: 40
description: >
  This topic provides information to get started with NetWitness Detect AI.
tags:
  - NetWitness
  - Logs
  - Logstash
  - Integration
---

## Getting Started with Detect AI

To onboard NetWitness Detect AI, existing customers with NetWitness Platform version 11.5.2 or later can share their tenant administrative user details with the NetWitness Sales team. The NetWitness Sales team then onboards the first administrative user from your organization to kick-start the set up process. The administrative user then receives a welcome email that contains the NetWitness Detect AI access URL, a user name, and a temporary password. Ensure that you reset the password at the first login.

The following checklist includes the steps to set-up and use NetWitness Detect AI:

## Before you Begin 

1. Ensure that you configure the actual time on the Cloud Link Service (Log Decoder Host). Sync the device Network Time Protocol (NTP) with the NTP service on the admin server. For more information on how to configure NTP Sever, see [Configure NTP Servers](https://community.netwitness.com/t5/netwitness-platform-online/configure-ntp-servers/ta-p/669729).

2. The host on which the Cloud Link Service will be installed needs to be connected to Amazon Web Services(AWS). This might require changes to your existing firewall rules. Hosts will need to connect to the IP ranges for the chosen deployment region. For more information on the current list of AWS IPs by region, see [AWS IP address ranges](https://docs.aws.amazon.com/general/latest/gr/aws-ip-ranges.html).
3. (Optional) Ensure that you configure the proxy settings from NetWitness Platform version 11.5.3 or later, before installing the Cloud link Service. For more information, see [Configure the proxy for the Cloud Link Service](/products/netwitnessdetectai/detectai/install-and-setup/proxy).

{{% table table-bordered %}}

Check | Task | Navigate To |
|:--- |:--- |:--- |
|<img src="/images/products/detectai/icons/checkbox.png" alt="checkbox" class="inline"/>|1. Understanding NetWitness Detect AI| [• NetWitness Detect AI Overview](/products/netwitnessdetectai/detectai/getting-started/detectai_overview) <br> [• What use cases does NetWitness Detect AI address](/products/netwitnessdetectai/detectai/getting-started/detectai_usecases) <br> [• Detect AI Work ](/products/netwitnessdetectai/detectai/getting-started/detectaiwork) <br> [•	Types of NetWitness Detect AI licenses](/products/netwitnessdetectai/detectai/getting-started/licensing) |
|<img src="/images/products/detectai/icons/checkbox.png" alt="checkbox" class="inline"/>|2. Log in to your account and perform the initial set up tasks| [• Log in to your account](/products/netwitnessdetectai/detectai/getting-started/loggingin) <br>  [• Set up and manage administrators](/products/netwitnessdetectai/detectai/getting-started/manage_administrators) <br> [• Enable multi-factor authentication for your account](/products/netwitnessdetectai/detectai/getting-started/mfa)|
|<img src="/images/products/detectai/icons/checkbox.png" alt="checkbox" class="inline"/>|3. Understanding Cloud Link Service| [Cloud Link Service Overview](/products/netwitnessdetectai/detectai/install-and-setup/overview_cloudlinkservice)|
|<img src="/images/products/detectai/icons/checkbox.png" alt="checkbox" class="inline"/>|4. Plan your Cloud Link Service installation| [Planning considerations for Cloud Link Service](/products/netwitnessdetectai/detectai/install-and-setup/planning_requirements)|
|<img src="/images/products/detectai/icons/checkbox.png" alt="checkbox" class="inline"/>|5. Install Cloud Link Service on Log Decoder (11.5.2 or later)| [Install Cloud Link Service](/products/netwitnessdetectai/detectai/install-and-setup/cloudlinkservice-install/#install-cloud-link-service)|
|<img src="/images/products/detectai/icons/checkbox.png" alt="checkbox" class="inline"/>|6. Download the activation package|[Download the activation package](/products/netwitnessdetectai/detectai/install-and-setup/cloudlinkservice-install/#download-the-activation-package) <br>|
|<img src="/images/products/detectai/icons/checkbox.png" alt="checkbox" class="inline"/>|7. Register the Cloud Link Service|[Register the Cloud Link Service](/products/netwitnessdetectai/detectai/install-and-setup/cloudlinkservice-install/#register-the-cloud-link-service)|
|<img src="/images/products/detectai/icons/checkbox.png" alt="checkbox" class="inline"/>|8. Verify if the Cloud Link Service is working|[Verify if the Cloud Link Service is working](/products/netwitnessdetectai/detectai/install-and-setup/cloudlinkservice-install/#verify-if-the-cloud-link-service-is-working)|
|<img src="/images/products/detectai/icons/checkbox.png" alt="checkbox" class="inline"/>|9. Enable data transfer from Detect AI to NetWitness Platform| [Transfer Detect AI data to NetWitness platform](/products/netwitnessdetectai/detectai/install-and-setup/cloudlinkservice-install/#transfer-detect-ai-data-to-netwitness-platform)|
|<img src="/images/products/detectai/icons/checkbox.png" alt="checkbox" class="inline"/>|10. Monitor Cloud Link Service |[Monitor the health of the Cloud Link Service](/products/netwitnessdetectai/detectai/install-and-setup/monitor_health)|
|<img src="/images/products/detectai/icons/checkbox.png" alt="checkbox" class="inline"/>|11. (Optional) Enabling email and syslog notifications for Cloud Link Service|[Configure email or syslog notifications to monitor the service](/products/netwitnessdetectai/detectai/install-and-setup/email_sylog_notifications)|
|<img src="/images/products/detectai/icons/checkbox.png" alt="checkbox" class="inline"/>|12. Updating the Cloud Link Service automatically|[Update the Cloud Link Service automatically](/products/netwitnessdetectai/detectai/install-and-setup/updatesensor)|
|<img src="/images/products/detectai/icons/checkbox.png" alt="checkbox" class="inline"/>|13. (Optional) Delete Cloud Link Service if no longer required|[Delete Cloud Link Service](/products/netwitnessdetectai/detectai/install-and-setup/uninstall_cloudlinkservice)|
|<img src="/images/products/detectai/icons/checkbox.png" alt="checkbox" class="inline"/>|14. Install Detect AI with an existing on-premise UEBA |[Install Detect AI with an existing on-premise UEBA](/products/netwitnessdetectai/detectai/install-and-setup/uebadetectai)|
|<img src="/images/products/detectai/icons/checkbox.png" alt="checkbox" class="inline"/>|15. (Optional) Configure proxy setting for the Cloud Link Service|[Configure the proxy for Cloud Link Service](/products/netwitnessdetectai/detectai/install-and-setup/proxy)|


After completing the set-up, you can perform several tasks to respond to threats reported by NetWitness Detect AI. For more information, see [Investigate](/products/netwitnessdetectai/detectai/investigate).

## Video - Setting up Detect AI 

{{< youtube EaJ_tjZkpm8>}}
