---
title: Setup and Manage Administrators 
description: This topic provides information about how to set up administrators for your account, add, edit, remove, enable or disable the administrator users.
weight: 90
---

## Setup and Manage Administrators 

Once the tenant administrative user of an organization is onboarded into NetWitness Detect AI, the administrative user can perform the following tasks:

+ Manage other administrative users - add, delete, enable and disable administrators, and update the profiles.
+ Install, configure, and manage sensors.
+ Configure and manage multi-factor authentication (MFA) for administrators.
+ Temporarily enable or disable access to other administrators, instead of deleting them permanently.


Use the following table as a guide to the user management tasks that you can perform.

**User Management Tasks in NetWitness Platform on the Cloud**

{{% table table-bordered %}}

Task | Description |
|:--- | :--- |
| Add an administrator | See [Add Additional Administrators](#add-additional-administrators) |
| Edit account settings | See [Edit User Account Settings](#edit-user-account-settings)  |
| Delete an administrator | See [Remove an Administrator](#remove-an-administrator)|
| Multi-factor user authentication | See [Enable Multi-Factor Authentication](/products/netwitnessdetectai/detectai/getting-started/mfa) |

### Add Additional Administrators

**To add an administrative user**

1. Go to <img src="/images/products/detectai/icons/admin3.png" alt="admin icon" class="inline"/> **Admin** > **Users Management** > **Users**.

   The Users and Roles page is displayed.
2. Click **Add User**.

   The Add User window is displayed.
3. Enter your first name, last name, email ID, and mobile number in the respective fields.
4. Click **Add**.


### Edit User Account Settings

As an administrator, you can update the user account settings for the administrators who are configured in the system. You must ensure that the contact information of administrative users is specified so that the user receives notifications on this contact number.

{{% notice note %}}
The mobile number you specify here must be valid as it will be used for multi-factor authentication for the user. For more information on multi-factor authentication, see [Enable Multi-Factor Authentication]({{< ref "/detectai/getting-started/mfa" >}}).
{{% /notice %}}

**To edit the administrator account settings**

1. Go to <img src="/images/products/detectai/icons/admin3.png" alt="admin icon" class="inline"/> **Admin** > **Users Management** > **Users**.

   The Users and Roles page is displayed.
2. Select the user, and click **Edit Details**.

   The Edit Details page is displayed.
3. Edit the first name, last name, and mobile number of the user in the respective fields.
4. Click **Save**.

If you are logged in and you want to edit your contact information, update your user profile by going to
**User Account** > **Profile**.

### Remove an Administrator 

As an administrator, you can remove the account details and access privileges for other administrators.

**To delete an administrator**

1. Go to <img src="/images/products/detectai/icons/admin3.png" alt="admin icon" class="inline"/> **Admin** > **Users Management** > **Users**.

   The Users page is displayed.
2. Click **Delete User**.

### Enable or Disable Access for Users

You can enable or disable access for users. When you disable access for a specific user, the user cannot access the NetWitness Platform on the Cloud account.

If a user is logged in to NetWitness Platform on the Cloud and the user access is disabled, the user can continue to access NetWitness Platform on the Cloud until the session times out.

**To enable access for a user**

1.	Log in to NetWitness Platform on the Cloud.
2.	Go to <img src="/images/products/detectai/icons/admin3.png" alt="admin icon" class="inline"/> **Admin** > **Users Management** > **Users**.
3.	Under the **Users** tab, select a user and click **Enable User**.
4.	To confirm, click **Enable**.

**To disable access for a user**

1.	Log in to NetWitness Platform on the Cloud.
2.	Go to <img src="/images/products/detectai/icons/admin3.png" alt="admin icon" class="inline"/> **Admin** > **Users Management** > **Users**.
3.	Under the **Users** tab, select a user and click **Disable User**.
4.	To confirm, click **Disable**.




