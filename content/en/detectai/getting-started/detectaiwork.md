---
title: Detect AI Work
description: This topic provides information about how Detect AI works.
weight: 55
toc: true
tags:
  - NetWitness
  - Logs
  - Event Source
  - Config Guide
  - Amazon Web Services
  - CloudTrail
---


## Detect AI Work

The analytics engine in NetWitness® Detect AI automatically monitors user behavior and utilizes advanced analytics to detect anomalies and risky behaviors. The analytics engine provides detailed analysis of user behavior, which enables analysts to review, investigate, monitor, and act on the identified risky behaviors.

The following table shows the steps that the alerting engine performs to derive the user behavioral results.

{{% table table-bordered %}}

Step | Description |
|:--- |:--- | :--- |
| 1. | Retrieve NetWitness Platform Data | The analytics engine retrieves the raw event data and metadata keys from the Concentrators and Brokers in NetWitness® Platform. The analytics engine processes and analyzes this data to create analytic results.|
| 2. | Create Baselines | *Baselines* are derived from a detailed analysis of normal user behavior and are used as a basis for comparison to user behavior over time. An example of the baseline for a user can include information about the time a user typically logs in to the network. |
| 3. | Detect Anomalies |  An *anomaly* is a deviation from a user’s normal baseline behavior. The analytics engine performs statistical analysis to compare each new activity to the baseline. User activities that deviate from the expected baseline values are scored accordingly to reflect the severity of the deviation. Anomalous activities are user behavior or abnormal user activities such as suspicious user logons, brute-force password attacks, unusual user changes, and abnormal file access. |
| 4. |Generate Alerts |  The anomalies detected in the previous step are grouped into hourly batches by the username. Each batch is scored based on the uniqueness of its *Indicators*, which define validated anomalous activities. If the indicator composition is unique compared to a user’s historic hourly batch compositions, it is likely that this batch will be transformed into an alert and its anomalies into indicators. A high-scored batch of anomalies becomes an alert that contains validated indicators of compromise. |
| 5. | Prioritize Users with Risky Behaviors |  The analytics engine prioritizes the potential risk from a user by using a simplified additive scoring formula. Each alert is assigned a severity that increases a user’s risk score by a predefined number of points. Users with high scores either have multiple alerts associated with them or they have alerts with high severity levels associated with them. |

