---
title: Detect AI Use Cases
description: This topic provides information about what use cases does NetWitness Detect AI address.
weight: 50
---

## Detect AI Use Cases

NetWitness Detect AI focuses on providing advanced detection capabilities to alert organizations about suspicious and anomalous user behavior. These behaviors could represent a malicious insider abusing credentials and access or could represent an external threat actor exploiting compromised credentials and systems.

Identity theft typically begins with the theft of credentials, which are then used to obtain unauthorized access to resources and to gain control over the network. Attackers may also exploit compromised non-admin users to obtain access to resources for which they have administrative rights, and then escalate those privileges.

An attacker who uses stolen credentials might trigger suspicious network events while accessing resources. Detecting illicit credential use is possible, but requires the separation of attacker activity from the high volume of legitimate events. NetWitness Detect AI helps you differentiate possible malicious activity from the otherwise abnormal, but not risky, user actions.

The use cases shown in the [Alert Types for Detect AI]({{< ref "/detectai/investigate/alerttypes" >}}) list define certain risk types and the corresponding system capabilities used for their detection. It is important that you review the use cases, represented by their alert type and description, to gain an initial understanding of the related risky behavior of each use case.

Using NetWitness Detect AI, you can then drill down into the indicators that reflect potential risky user activities to learn more. For more information about supported indicators, see [Alert Types for Detect AI]({{< ref "/detectai/investigate/alerttypes" >}}).