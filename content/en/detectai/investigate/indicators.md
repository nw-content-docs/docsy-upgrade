---
title: Indicators for Detect AI
description: This topic provides information about the different indicators that are generated for Detect AI.
weight: 50
toc: true
tags:
  - NetWitness
  - Logs
  - Event Source
  - Config Guide
  - Amazon Web Services
  - CloudTrail
---


## Indicators for Detect AI

An *Indicator* is a validated anomaly, which is different from the typical or baseline behavior of the user. The following tables list indicators that display in the user interface when a potentially malicious activity is detected for users.


## Windows File Servers

{{% table table-bordered %}}
Indicator | Alert Type  | Description |
|:--- | :--- |:--- 
| Abnormal File Access Time | Non-Standard Hours | A user has accessed a file at an abnormal time.|
| Abnormal File Access Permission Change | Mass Permission Changes | A user changed multiple share permissions.|
| Abnormal File Access Event | Abnormal File Access | A user has accessed a file abnormally.|
| Multiple File Access Permission Changes | Mass Permission Changes | A user changed multiple file share permissions.|
| Multiple File Access Events | Snooping User | A user accessed multiple files.|
| Multiple Failed File Access Events | Snooping User | A user failed multiple times to access a file.|
| Multiple File Open Events | Snooping User | A user opened multiple files.|
| Multiple Folder Open Events | Snooping User | A user opened multiple folders.|
| Multiple File Delete Events | Abnormal File Access | A user deleted multiple files.|
| Multiple Failed File Access Permission Changes | Mass Permission Changes | A user failed multiple attempts to change file access permissions.|

## Active Directory

{{% table table-bordered %}}
Indicator | Alert Type  | Description |
|:--- | :--- |:--- 
| Abnormal Active Directory Change Time | Non-Standard Hours | A user made Active Directory changes at an abnormal time.|
| Abnormal Active Directory Object Change | Abnormal AD Changes | A user made Active Directory attribute changes abnormally.|
| Multiple Group Membership Changes | Mass Changes to Groups | A user made multiple changes to groups successfully.|
| Multiple Active Directory Object Changes | Abnormal AD Changes | A user made multiple Active Directory changes successfully.|
| Multiple User Account Changes | Abnormal AD Changes | A user made multiple sensitive Active Directory changes successfully.|
| Multiple Failed Account Changes | Abnormal AD Changes | A user failed to make multiple Active Directory changes.|
| Admin Password Changed | Admin Password Change | The password of an admin was changed.|
| User Account Enabled | Sensitive User Status Changes | An account of a user was enabled.|
| User Account Disabled | Sensitive User Status Changes | An account of a user was disabled.|
| User Account Unlocked | Sensitive User Status Changes | An account of a user was unlocked.|
| User Account Type Changed | Sensitive User Status Changes | The type of user was changed.|
| User Account Locked| Sensitive User Status Changes | An account of a user was locked.|
| User Password Reset| Sensitive User Status Changes | The password of a user was reset.|
| User Password Never Expires Option Changed| Sensitive User Status Changes | The password policy of a user was changed.|

## Logon Activity

{{% table table-bordered %}}
Indicator | Alert Type  | Description |
|:--- | :--- |:--- 
| Abnormal Remote Host | Logon to Abnormal Remote Host | A user attempted to access a remote computer abnormally.|
| Abnormal Logon Time | Non-Standard Hours | A user logged on at an abnormal time.|
| Abnormal Host | User Logon to Abnormal Host | A user attempted to access a host abnormally.
| Multiple Successful Authentications | Multiple Logons by User | A user logged on multiple times.
| Multiple Failed Authentications | Multiple Failed Logons | A user failed multiple authentication attempts.
| Logon Attempts to Multiple Source Hosts | User Logged into Multiple Hosts | A user attempted to log on from multiple computers.
| Abnormal VPN Logon Time | Non-Standard Hours | A user has logged on at an abnormal time.
| Abnormal VPN Logon Country* | Abnormal Logon Country | A user attempted to establish VPN access from an abnormal country.
| Multiple Failed VPN Authentications | Multiple Failed VPN Logons | A user failed multiple times to authenticate for VPN access.
| Abnormal Azure AD Logon Time | Non-Standard Hours | A user has logged on at an abnormal time.
| Abnormal Azure AD Logon Country* | Abnormal Logon Country | A user attempted to access Azure AD from an abnormal country.
|Multiple Failed Azure AD Authentications | Multiple Failed Logons | A user failed multiple times to authenticate into Azure AD.
| Azure AD - Abnormal Application | Abnormal Remote Application | A user attempted to log on to abnormal number of applications   through Azure AD.
| Azure AD - Logon Attempts to Multiple Applications | Snooping User - Cloud Service Account | A user attempted to log on to multiple applications through Azure AD.

{{% notice note %}}
*For Abnormal Azure AD Logon Country, it is recommended to dynamically update the GeoIP repository to obtain optimal results.
{{% /notice %}}
### Process ###

{{% table table-bordered %}}
Indicator | Alert Type  | Description |
|:--- | :--- |:--- 
| Abnormal Process Created a Remote Thread in LSASS | Credential Dumping | An abnormal process was created into the LSASS process.|
| Abnormal Reconnaissance Tool Executed | Discovery and Reconnaissance | An abnormal process was executed. |
| Abnormal Process Executed a Scripting Tool | PowerShell and Scripting | An abnormal process executed a scripting tool.
| Abnormal Process Executed a Scripting Tool | PowerShell and Scripting | An abnormal process was triggered by a scripting tool.
| Scripting Tool Triggered an Abnormal Application | PowerShell and Scripting | An abnormal process was opened by a scripting tool.
| Abnormal Process Created a Remote Thread in a Windows | PowerShell and Scripting | An abnormal process was injected into a known windows process.
| Multiple Distinct Reconnaissance Tools Executed| Discovery and Reconnaissance| Multiple reconnaissance tools were executed in an hour.
| Multiple Reconnaissance Tool Activities Executed | Discovery and Reconnaissance | Multiple reconnaissance tool activities were executed in an hour.
| User Ran an Abnormal Process to Execute a Scripting Tool | PowerShell / Scripting | An abnormal process executed a scripting tool.
| User Ran a Scripting Tool that Triggered an Abnormal Application | PowerShell / Scripting | A scripting tool was executed that triggered an abnormal application.
| User Ran a Scripting Tool to Open an Abnormal Process| PowerShell / Scripting | A scripting tool was executed to open an abnormal process.

### Registry ###

{{% table table-bordered %}}
Indicator | Alert Type  | Description |
|:--- | :--- |:--- 
| Abnormal Process Modified a Registry Key Group | Registry Run Keys | An abnormal process modified a service key registry.|


