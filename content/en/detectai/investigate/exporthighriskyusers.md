---
title: Export a List of High-risk Users
description: This topic provides information about how to export a list of high-risk users.
weight: 130
---

## Export a List of High-risk Users

You can export a list of all users and their scores in a .csv file format. You can use this information to compare with other data analysis tools like tableau, powerbi, and zeppelin.

1. Log in to NetWitness Platform XDR and click **Users**.

   The Overview tab is displayed.

2. Click **Entities** tab.
3. Click **Export**.
   ![High-Risk-Users](/images/products/detectai/Expo.png)