---
title: Investigate Events 
description: This topic provides information about how to investigate events.
weight: 100
toc: true
tags:
  - NetWitness
  - Logs
  - Event Source
  - Config Guide
  - Amazon Web Services
  - CloudTrail
---

## Investigate Events 

You can view all alerts and indicators associated with a user in the User Profile view. In the events table, you can find the events that contributed to a specific indicator for a specific user.
You can further investigate on events by clicking on a username that pivots to **Investigate** > **Events**. In the Events view, you can see the list of events that occurred on that day for the specific user. By default, the time range is set to one hour. You can change the time range.

**To Investigate Events**

1. Go to **Users** > **Alerts**.
2. Under **Filters**, select the **Entity Type** as **Users**. 
   The Alerts are displayed, along with the anomaly value, data source, and start time.
   ![Alert Flow](/images/products/detectai/AlertFlow2.png)

3. Click an alert name, and under **Alert Flow**, click the **+** icon. 

   A graph is displayed that shows details about a specific indicator, including the timeline in which the anomaly occurred and the user associated with the indicator. The following figure shows an example of a graph. The type of graph can vary, depending on the type of analysis performed by NetWitness.
   ![Alert Graph](/images/products/detectai/AlertGraph2.png)