---
title: Filter an Alert
description: This topic provides information about how to filter an alert.
weight: 200
---

## Filter an Alert

You can filter alerts to retrieve alert details using specific parameters to help further investigation. They are displayed in the Alerts tab by severity, feedback, indicators, and date range.

1. Go to **Users** > **Alerts**.
   The Alerts tab is displayed.
   <div>
      <img src="/images/products/detectai/AlertTab.png" alt="Alerts Tab" width="900" height="min">
   </div>
   
2. To filter by severity, click the down arrow under **Severity** in the **Alerts Filters** panel, and select any one option. The options are Critical, High, Medium, and Low.
3. To filter by feedback marked as **Not a Risk**, click the down arrow under **Feedback**, and select the **Rejected** option.
4. To filter by entity, click the down arrow under **Entity Type**, and select **Users** option.
5. To filter by date range, click the down arrow under **Date Range** and select an option. The Options are Last 24 Hours, Last 7 Days, Last 1 Month, and Last 3 Months.
   The alerts are displayed in the right panel according to the selected filter. To reset filters, click **Reset**, in the bottom of left panel.