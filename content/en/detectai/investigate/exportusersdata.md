---
title: Export User Data
description: This topic provides information about how to export user data.
weight: 220
---

## Export User Data

You can export a list of all users and their scores in a .csv file format. You can use this information to compare with other data analysis tools like tableau, powerbi, and zeppelin.

**To export alert data**

1. Log in to NetWitness Platform XDR.
2. Go to **Users** > **Alerts**. 

   The Alerts tab is displayed with the alert data.
3. Click **Export**.
   ![Export User Data](/images/products/detectai/export.png)