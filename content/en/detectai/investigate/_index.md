---
title: Investigate
linkTitle: "Investigate"
description: >
  Provides information about how Analysts uses Detect AI to identify and respond to threats.
---
