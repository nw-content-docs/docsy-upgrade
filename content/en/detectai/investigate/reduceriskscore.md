---
title: Reduce User Risk Score
description: This topic provides information about how to reduce the risk score.
weight: 90
toc: true
tags:
  - NetWitness
  - Logs
  - Event Source
  - Config Guide
  - Amazon Web Services
  - CloudTrail
---

## Reduce User Risk Score

If an alert is not a risk, you can mark it so that the user score is automatically reduced.

1. Log in to NetWitness Platform XDR and click **Users**.
2. In the **Overview** tab, under **Top Risky Users** panel, click on a username.

   The User Profile view is displayed.

3. Select the alert, click **Not a Risk**.
   <div>
      <img src="/images/products/detectai/NotRis_1.png" alt="Reduce the User Risk Score" width="900" height="min">
   </div>