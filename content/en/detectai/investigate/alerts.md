---
title: Identify Critical Alerts
description: This topic provides information about identifying the critical alerts.
weight: 170
toc: true
tags:
  - NetWitness
  - Logs
  - Event Source
  - Config Guide
  - Amazon Web Services
  - CloudTrail
---

## Identify Critical Alerts 

Anomalies that are found as incoming events are compared to the baseline and compiled into hourly alerts. Relatively strong deviations from the baseline, together with a unique composition of anomalies, are more likely to get a higher alert score.
You can quickly view the most critical alerts in your environment, and start investigating them from either the Overview tab or the Alerts tab. The following figure is an example of top alerts in the Overview tab. The alerts are listed in order of severity and the number of indicators who generate the alerts.
<div>
      <img src="/images/products/detectai/AlertTab.png" alt="Alerts Tab" width="900" height="min">
   </div>
Here you can quickly view all the critical alerts, filter them based on date range and criticality in your environment, and start investigation.

**To identify such alerts**

1. Log in to NetWitness Platform.
2. Go to **Users** > **Alerts**. 

   The Alerts tab is displays all the critical alerts.

3. In the filters panel, do the following:

   * In the **Severity** drop-down, select **Critical**.
   * In the **Date Range** drop-down, select the date range. The options are Last 24 Hours, Last 7 Days, Last 1 Month, and Last 3 Months. By default, last 3 Months alerts are displayed.
   * If you want to set a unique date range, select the Custom Date under Date Range and specify the Start Date and End Date that you want the investigate.
   The alerts are displayed in the right panel according to the filter you selected.



