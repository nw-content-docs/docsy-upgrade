---
title: Save a Filter
description: This topic provides information about saving a filter.
weight: 190
---

## Save a Filter

You can save a behavioral filter for future investigations and avoid entering the details every time. The behavioral profile is saved and displayed in the Favorites panel. You can click on the profile in the Favorites to monitor the users.

**To save a filter**

1. Log in to NetWitness Platform XDR.
2. Go to **Users** > **Entities**.

   The Overview tab is displayed.
   
3. Click **Entities** tab.
4. Enter the required details in the Filter panel on the left-side panel.
5. Click **Save as**. 
6. Enter a Filter Name in the Save as **Favorites** pop-up window.
7. Click **Save**.