---
title: Take Action on Users
description: This topic provides information about how to take action on users.
weight: 210
---

## Take Action on Users

After investigation, you can take action on the risky users to reduce or prevent further damage caused by malicious attackers in your organization. You can take any of the following actions: 

* Specify if the alert is not risky.
* Save the behavioral profile for the use case found in your environment.
* Add user profiles to the watchlist, if you want to keep a track of the user activity.
