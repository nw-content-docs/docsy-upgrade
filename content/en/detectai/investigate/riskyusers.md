---
title: Identify all Risky Users
description: This topic provides information about how to identify all risky users.
weight: 85
toc: true
tags:
  - NetWitness
  - Logs
  - Event Source
  - Config Guide
  - Amazon Web Services
  - CloudTrail
---


## Identify Risky Users 

1. Go to **Users** > **Entities**.

   The users list in the Entities view shows all the users monitored by SIEM Analytics. *Risky Users* are users with a risk score (risk score greater than 0). Risky users display abnormal behavior and can potentially compromise your organization.

2. For each user of interest, click the user in the list to open the user's profile. To investigate a user and drill further into the user behavior detail, see [Identify the Top Risky Users]({{< ref "/detectai/investigate/topriskyusers" >}}).


