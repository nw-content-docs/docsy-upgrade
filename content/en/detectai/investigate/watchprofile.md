---
title: Watch a Profile
description: This topic provides information about how to watch a profile.
weight: 120
---

## Watch a Profile

The watch user profile is a list of users that you want to monitor for potential threats. The watch user profile marks a user so that the users can be quickly referenced on the dashboard. This is essentially a bookmark to monitor suspicious users.

**To watch a user profile**

1. Log in to the NetWitness Platform XDR and click **Users**.
2. In the **Overview** tab, under **Top Risky Users** panel, click username.
3. Click **Watch Profile**.  
   
   The user is added to the watchlist.
   ![Profile](/images/products/detectai/WatPro.png)