---
title: Read an Indicator Chart
description: This topic provides information about how to read an Indicator Chart.
weight: 80
---

## Read an Indicator Chart

{{% notice note %}}
To view the dotted chart and display the data in an optimal way the on-premise version must be upgraded to 11.6 version or later.
{{% /notice %}}

An indicator chart is a pictorial illustration of the anomaly and baseline values of an entity that you want to further investigate.
The chart gives the Analyst a better insight of the indicator which in turn will help determine the next steps. The chart provides the analyst with the user’s baseline values over time to better understand the context of the anomaly.

**To view an indicator chart**

1. Log in to NetWitness Platform XDR.
2. Go to **Users** > **Entities**.
3. Select the user you want to investigate. 
   The following figure displays an alert for a user logged on to an abnormal host. <br>
   <div>
      <img src="/images/products/detectai/Chart1_1.png" alt="Chart 1" width="900" height="min">
   </div>
4. In the **Alert Flow** section, select the Logon Attempts to Multiple Source Hosts.
5. Click the **+** icon to expand and view the details.
   <div>
      <img src="/images/products/detectai/Chart2_1.png" alt="Chart 2" width="900" height="min">
   </div>

**Type of Charts**

There are three main types of charts currently available.

**Continuous Bar Chart**

In this type of an indicator chart, the bar color differentiates the behavior by displaying a blue bar and a red bar. For example, the following figure displays in a span of 30-days the number of files a Snooping User has attempted to access in an hour which are displayed by blue bars and indicates the baseline behavior.
The red bar indicates that the user has accessed a high number of files in a specific hour.  
<div>
      <img src="/images/products/detectai/Chart3_1.png" alt="Chart 3" width="900" height="min">
   </div>

Another variation in the visualization of the chart is where you see an additional series of grey bars that represents the baseline values of the model. In this case, if the blue bars series is displayed, it depicts the specific entity trend that the anomaly is also a part of.

**Dotted Chart**

In the dotted indicator chart, the anomaly is displayed on top of the graph indicated by yellow color text and red color circle.   The chart provides the analyst with the user’s baseline values over time to better understand the context of the anomaly. The additional values (apart from the anomaly value) depicted in the Y-axis, represent the baseline values and the total number of days they were observed for this specific entity.  
<div>
      <img src="/images/products/detectai/Chart4_1.png" alt="Chart 4" width="900" height="min">
   </div>

**Time Chart**

The time indicator chart displays the time the user has accessed a particular information. For example, in the following figure, the user has accessed the Active Directory at an abnormal time over the past 30 days. It displays the aggregate time spent on each day between 8.00 to 16.00. The baseline values are displayed with the regular working hours of the user and the anomaly value (the hour marked in red) to indicate that this is an abnormal time for this user to make changes in AD.
   <div>
      <img src="/images/products/detectai/Chart2_1.png" alt="Chart 3" width="900" height="min">
   </div>


