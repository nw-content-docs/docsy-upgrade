---
title: Filter Users for Investigation 
description: This topic provides information about how to filter users for investigation.
weight: 160
---

## Filter Users for Investigation 

In the **Entities** tab, you can use Alert Types and Indicators which are behavioral filters to view high-risk users. The behavioral profile is saved and displayed in the Favorites panel. You can click on the profile in the Favorites to monitor the users.

**To view users for investigation**

1. Log in to NetWitness Platfor XDR.
2. Go to **Users** > **Entities**. 

   The Overview tab is displayed.
2. Click **Entities** tab.
3. To create a behavioral filter using alert types, select one or more alerts in the **Alerts** drop-down list 4.
4. To create a behavioral filter using indicators, select one or more indicators in the **Indicators** drop-down list 5.
   <div>
      <img src="/images/products/detectai/EntFil.png" alt="Filter Users for Investigation" width="900" height="min">
   </div>