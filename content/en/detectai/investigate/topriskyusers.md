---
title: Identify Top Risky Users
description: This topic provides information about how to identify top risky users.
weight: 95
toc: true
tags:
  - NetWitness
  - Logs
  - Event Source
  - Config Guide
  - Amazon Web Services
  - CloudTrail
---

## Identify Top Risky Users

All users in your organization can be analyzed for abnormal user activities and assigned a user risk score. Users with high scores either have multiple alerts associated with them or they have high-level severity alerts associated with them. These scores and alerts enable you to quickly identify high-risk users so that you can investigate their abnormal activities in your environment.

The top risky users are users with the highest risk scores. A lot of alerts and high-severity alerts contribute to the score.

1. Go to **Users** > **Overview** and in the **Users** tab, look at the Top Risky Users panel on the left.
   <div>
      <img src="/images/products/detectai/topRiskyUsers1_1.png" alt="Alerts Risky Users" width="900" height="min">
   </div>

2. Look at the Top Risky Users, which are the top ten users with the highest risk scores.

    a. Look for high user scores marked with critical or high severity.

    b. Check if any user scores increased since yesterday. If you see +0, there was no increase since yesterday.

    c. Look for users with critical (red band) alerts.
   <div>
      <img src="/images/products/detectai/wh_riskyusers2_1.png" alt="Alerts Risky" width="650" height="min">
   </div>

In this example, Levi has a high user score of 112, and 2 critical alerts. Levi also has 2 high, 3 medium, and 12 low severity alerts. Charlie has a user score of 80 lower than Levi, but there are also 4 critical alerts. Looking at this information, it would be a good idea to further investigate the activities of both of these risky users.

3. Hover over the number of alerts associated with the risky users to quickly see the severity levels of the alerts associated with the users. 
   In this example, you can see that Levi has 2 critical, 2 high, 3 medium, and 12 low severity alerts.
   <div>
      <img src="/images/products/detectai/topriskyusers3_1.png" alt="Critical Users" width="650" height="min">
   </div>

4. You can click a risky user of interest in the list to open the user's profile.
   <div>
      <img src="/images/products/detectai/topriskyusers4_1.png" alt="Critical Users" width="650" height="min">
   </div>

   The user profile enables you to access detailed information on the anomalous behavior of the user, including the alerts associated with them and the indicators that generated those alerts.
   <div>
      <img src="/images/products/detectai/topRiskyUserProfile_1.png" alt="topRiskyUserProfile" width="900" height="min">
   </div>

5. See **Investigate a Risky User** to investigate the user and drill further into the user behavior details.
   <div>
      <img src="/images/products/detectai/topRiskyUsers1_1.png" alt="Top risky users" width="900" height="min">
   </div>
