---
title: Save a Behavioral Profile
description: How to monitor user behavior and utilize advanced analytics to detect anomalies and risky behaviors?
weight: 105
toc: true
tags:
  - NetWitness
  - Logs
  - Event Source
  - Config Guide
  - Amazon Web Services
  - CloudTrail
---


## Save a Behavioral Profile

The combination of the alert types and indicators you select during the forensics investigation is a behavioral profile. You can save the behavioral profile, so you can monitor this use case in future. For example, if in your organization a user attempted to login and failed multiple times, you can select filters using the multiple failed authentications alert type. This can be saved as favorite. You can proactively monitor for future brute force attempts. To do so, you can click the favorite to see if new users were subjected to this type of attack.

**To save a behavioral profile**

1. Log in to the NetWitness Platform XDR and click **Users**.

   The Overview tab is displayed.
   
2. Click **Entities** tab.
3. In the **Filters** panel, select the alert in the **Alerts** drop-down and Indicators in the **Indicators** drop-down.
   <div>
      <img src="/images/products/detectai/EntFil.png" alt="BehavioralProfile" width="900" height="min">
   </div>