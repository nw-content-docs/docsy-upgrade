---
title: View the Usual Behavior of a User
description: This topic provides information about how to view the usual behavior of a user.
weight: 140
---

## View the Usual Behavior of a User

NetWitness Detect AI Modeled Behavior provides analysts with visibility into the usual activities of users monitored by Detect AI. These modeled behaviors are based on the log data leveraged by Detect AI and are available a day after the Detect AI service is configured. Detect AI monitors abnormal user behaviors to identify risky users and this requires data to be processed for a certain period of time. However, Modeled Behaviors reflect the activities of the user within a day of the service configuration.
For example, if a user fails multiple times by logging in with incorrect credentials within an hour, analysts can view these behaviors as Failed Authentications for the user.

**To view the Modeled Behaviors**

1. Log in to NetWitness Platform XDR and click **Users**.
2. In the **Overview** tab, under **Top Risky Users** panel, click a username.
3. Click **Modeled Behaviors**, to view the Modeled Behaviors highlighted with a blue line in the left panel. The results can be sorted by the date or in alphabetical order.
   ![Pie Chart depiction of Modeled Behavior](/images/products/detectai/piechart_modeled.png)