---
title: Check the Activity of a Specific User
description: This topic provides information about checking the activity of a specific user.
weight: 150
---

## Check the Activity of a Specific User

You can view all alerts and indicators associated with a user in the User Profile view. In the events table, you can find the events that contributed to a specific indicator for a specific user. You can further investigate on events by clicking on a username that pivots to **Investigate** > **Events**. In the Events view, you can see the list of events that occurred on that day for the specific user. By default, the time range is set to one hour. You can change the time range.

**To check the activities of a specific user**

1. Log in to NetWitness Platform.
   Go to **Users** > **Alerts**.
2. Under **Filters**, click **Users**.
3. Select a specific user.
   ![Check the activity of a specific user](/images/products/detectai/SpfUser.png)
