---
title: Investigate Alerts
description: This topic provides information about how to investigate alerts.
weight: 180
---

## Investigate Alerts

1. Log in to NetWitness Platform XDR and go to **Users**.

   The overview tab is displayed.

2. In the **Overview** tab, look at Alert Severity panel.
   Is there an even distribution of alerts or are there a few days when there was a noticeable spike? A spike could indicate something suspicious like malware. Make a note of those days so you can inspect the alerts (the bar from the chart links directly to the alerts for that specific day).
3. Click Critical Alerts date range.
   ![Critical Alerts](/images/products/detectai/alertseverityHig.png)

   The Alerts tab is displayed.
   <div>
      <img src="/images/products/detectai/AlertTab.png" alt="Alerts Tab" width="900" height="min">
   </div>
   
4. In the **Alerts** tab, you can view the indicator count to identify users with the highest number of alerts, more indicators help illustrate more insights and     
   provide a more rigid timeline that you can follow:
   * Expand the top alerts in the list.
   * Look for alerts that have varied data sources. These show a broader pattern of behavior.
   * Look for a variety of different indicators.
   * Look for indicators with high numeric values, specifically for high values that are not indicative of a manual activity (for example, a user accessed 8,000 files).
   * Look for unique Windows event types that users do not typically change as these can indicate suspicious administrative activity.
5. Search by indicators. The list shows the number of alerts raised that contain each indicator.
   * Look for the top volume indicators; filter by an indicator and review by user to find users who experienced the highest number of these indicators.
   * In general, as they are common time-based alerts (for example, Abnormal Logon Time), they can provide good context when combined with higher interest indicators.
6. Drill into more detail:
   ![Drill into further details](/images/products/detectai/LevAler.png)
   * Leverage alert names to begin establishing a threat narrative. Use the strongest contributing indicator that usually determines the alert’s name to begin explaining why this user is flagged.
   * Use the timeline to layout the activities found and try to understand the observed behaviors.
   * Follow up by reviewing each indicator and demonstrating the supporting information, in the form of graphs and events, that can help you verify an incident. Suggest possible next stages of investigation using external resources (for example, SIEM, network forensics, and directly reaching out to the user, or a managing director).


   