---
title: What is Happening now in your Organization
description: This topic provides information about What is happening now in your organization.
weight: 75
toc: true
tags:
  - NetWitness
  - Logs
  - Event Source
  - Config Guide
  - Amazon Web Services
  - CloudTrail
---

## What is Happening now in your Organization

### Workflow Overview

The Users Overview view shows what is happening in your environment at a glance. NetWitness Detect AI enables you to quickly determine potential malicious activity, investigate it further, detect anomalies, and take action.
<div>
      <img src="/images/products/detectai/whatshapp_1.png" alt="Workflow Overview" width="1050" height="min">
   </div>


### Top Risky Users

In this view you can  look at the top ten users listed, which are the top ten users with the highest user risk scores. The circled user indicates high score and severity. Compare and see if any user scores have increased since the previous day. Also, investigate users with critical alerts.
![Top Risky Users](/images/products/detectai/topRiskyUsers2.png)

### Use Case Scenario

In the above example, Levi Thomas has a user score of 132, which is over 100, and 3 critical alerts. Charlie Martin has a user score of 80, which is not over 100, but Charlie has 4 critical alerts. (All of the top ten users listed show +0 next to their score, so the scores did not increase since yesterday.)
In the Top Alerts panel, look at the top alerts for Users in the last 24 hours or a later time period if you do not see any alerts.

1. Check the alerts by severity level, starting with the critical alerts. What type of alerts are they? Which users are associated with the alerts?
2. Check for alerts with a high number of indicators (anomalies).
3. To view the specific indicators associated with an alert, hover over the number of indicators listed.

### Alerts View 

In this example, the Top Alerts panel shows four Snooping User critical alerts shown for user Charlie Martin in the last 3 months. Hovering over "3 indicators" for one of the alerts shows the names of the indicators of compromise in the alert: Multiple File Access Events, Multiple File Delete Events, and Abnormal File Access Event.

In the above example, user Charlie Martin has one critical Snooping User alert containing 3 indicators in the last 3 months. 
![Alerts View](/images/products/detectai/WH_Alerts_3.png)

### Severity View

In the Alerts Severity panel, look at when the critical alerts happened in the last three months. 
In this example, the majority of the alerts in the last three months occurred on the same day.
![Severity View](/images/products/detectai/wh_Severity4.png)

### All Alerts View
If you click on this day, it opens the Alerts view, where you can drill down into the alerts from the selected day.
<div>
      <img src="/images/products/detectai/wh_AlertsView.png" alt="All Alerts View" width="950" height="min">
   </div>

## Snooping Alerts 
If you go back to the Top Risky Users panel (Users &gt; Overview), you can drill further into the alerts listed for each of the top risky users.
For example, Charlie's user profile shows Snooping User alerts and provides details of multiple files accessed and deleted.
<div>
      <img src="/images/products/detectai/wh_SnoopingUserProfile1.png" alt="User Profile 2" width="950" height="min">
   </div>
<br>

## Data Retention

NetWitness retains any inactive users with no incoming data for six months. NetWitness removes the user's data and any associated alerts from the system after six months.
