---
title: Overview on DetectAI
linkTitle: Overview
description: >
  This topic provides information about Detect AI.
draft: false
weight: 1
---

## Overview

NetWitness Detect AI is an add-on to NetWitness® Platform. The product is a SaaS solution that analyzes NetWitness Platform data and triggers alerts on potential threats and malicious activity. NetWitness Detect AI offers two solutions:

- [Overview](#overview)
  - [Detect AI](#detect-ai)
  - [Detect AI Insight](#detect-ai-insight)

### Detect AI

NetWitness Detect AI (Logs and Endpoints) takes all the traditional functions of NetWitness User Entity Behavior Analytics (UEBA) and provides them as a native SaaS application. As a cloud service, Detect AI has many additional benefits including operations from the Operations team who manage the service for your organization which enables to release new content and enhancements faster so security teams are better equipped to respond to threats.

NetWitness Detect AI is an advanced analytics and machine learning solution that leverages unsupervised machine learning and empowers Security Operations Center (SOC) teams to discover, investigate, and monitor risky behaviors in their environment. All users in an organization can be analyzed for abnormal user activities using log and endpoint data already collected by your NetWitness Platform.

For existing NetWitness Platform customers, NetWitness Detect AI enables analysts to:

* Detect malicious and rogue users
* Pinpoint high-risk behaviors
* Discover attacks
* Investigate emerging security threats
* Identify potential attacker's activity

NetWitness Detect AI resides on an Amazon Virtual Private Cloud (VPC) and each organization has its own VPC. If you have an on-premises NetWitness Platform deployment in your network, metadata will be uploaded to the cloud for analysis.

NetWitness Detect AI performs advanced analytics on the data to enable analysts to discover potentials threats. Analysts will begin to see alerts and behavior profiles of users directly in their existing NetWitness Platform UI, and will be able to perform basic administration of the SaaS components from a dedicated SaaS UI.

For more information, see [Detect AI]({{< ref "/detectai" >}})

### Detect AI Insight

NetWitness Detect AI Insight is a SaaS solution available as an extension for a NetWitness Network, Detection & Response (NDR) customer. Detect AI Insight is an advanced analytics solution that leverages unsupervised machine learning to empower the response of the Security Operations Center (SOC) team. Detect AI Insight continuously examines network data collected by the Decoder to discover, profile, categorize, characterize, prioritize, and track all assets actively.

NetWitness Detect AI Insight passively identifies all assets in the enviornment and alerts analysts of their presence. The discovered assets are automatically categorized into groups of similar servers and prioritized based on their network profiles. These assets are presented to analysts to guide them to focus on certain assets to protect their organization.

Detect AI Insight enables you to do the following:

+ Asset discovery and characterization.
+ Monitor critical Assets.
+ Leverage the security operations team to triage based on prioritization.

For more information, see [Detect AI Insight]({{< ref "/detectaiinsight" >}})