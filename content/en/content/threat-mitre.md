---
title: NetWitness Threat Content (by MITRE ATT&CK Technique)
linkTitle: Threat Content (by MITRE ATT&CK)
description: >
  Visualize the capabilities of NetWitness Threat detection based on MITRE ATT&CK Tactics and Techniques
draft: false
weight: 11
layout: atkmatrix
---