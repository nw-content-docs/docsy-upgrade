---
title: View Subscription Details
description: View subscription details
weight: 945
---

## View Subscription Details

You can view the following details of the NetWitness IoT subscription:
* License information such as active licenses and license expiry.
* Used and available storage space based on your subscription.
* Storage History information such as usage statistics for a period.

**To view your subscription details**

1.	Log in to NetWitness IoT.
2.	On the top right, click **Subscription**.
![NetWitness IoT Subscription](/images/products/iot/subscription.png)














