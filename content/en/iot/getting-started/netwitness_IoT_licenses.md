---
title: About NetWitness IoT licenses
description: About NetWitness IoT licenses
weight: 922
---

## About NetWitness IoT licenses

NetWitness IoT licenses are valid for the time period associated with the license purchase. NetWitness IoT provides a customer-focused licensing strategy.

The following pricing is annual and can be billed monthly:

{{% table table-bordered %}}

Product |  Price  |  Unit  |  Storage  |  Duration
|:--- |:--- |:--- |:--- |:---
|  NetWitness IoT (SaaS only) |  | Entitles full application with cloud UI for 12 months | .5 TB storage | Annual |
| NetWitness IoT (SaaS only) |   | Per add-on capacity unit | 1 TB storage | Annual |

{{% /table %}}

When an extra capacity unit is added, it is prorated on a monthly basis according to the contract cycle for the primary unit. Contact support to learn more about subscription details and licenses.