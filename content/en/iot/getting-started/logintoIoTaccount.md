---
title: Log in to your NetWitness IoT
description: Log in to your NetWitness IoT
weight: 927
---

## Log in to your NetWitness IoT

NetWitness IoT is a cloud solution, and you need to log into your NetWitness IoT to view the alerts and perform the tasks.

**To log in to your NetWitness IoT account**
1.	Open your browser and type the following URL:

    <https://iot.netwitness.com>
2.	Click **LOGIN** and type your email address and password, and then click **Sign in**.

    The Dashboard page is displayed.
    ![NetWitness IoT Sign In](/images/products/iot/login_page.png)
