---
title: Welcome to NetWitness IoT
description: Welcome to NetWitness IoT
weight: 920
---

## Welcome to NetWitness IoT

Every enterprise is adding new IoT devices as part of improving its infrastructure and operation efficiency. The IoT devices include sensors, robots, smart locks, connected cameras, mobile card readers, inventory devices, stock control devices, and other smart devices. The wide range of complex functions performed by IoT devices makes it hard for security teams to spot anomalous behavior.
This poses a considerable challenge in securing these new IoT devices and protecting the infrastructure and data. NetWitness IoT provides complete visibility into your connected IoT environment.

NetWitness IoT enables organizations to extend their Security Operation Center (SOC) visibility beyond IT and into IoT and OT (Operational Technology).

NetWitness IoT provides the ability to monitor and detect threats end-to-end. For example, an attack initiated from the IT systems and laterally moved to the OT systems can be easily detected. The analysts can get visibility into the complete spectrum of attacks on the entire environment.

NetWitness IoT uses the following technologies to identify security threats and detects compromised IoT devices quickly:

+ Use of latest technology to quickly identify when IoT devices are compromised.
+ Latest behavior analytics.
+ Advanced threat intelligence.


NetWitness IoT transforms organizations by providing the following capabilities:

{{% table table-bordered %}}
Features |  Benefits
|:--- |:--- |
|  Visibility across the edge and IoT devices using machine learning and behavioral alerting | NetWitness IoT provides enhanced visibility on your IoT devices and alerts you in case of any threats or anomalous activities. The wide range of complex functions performed by IoT devices makes it hard for security teams to spot anomalous behavior. NetWitness IoT applies advanced machine learning and behavior analytics, along with advanced threat intelligence, to quickly identify instances where devices could be compromised.|
| Robust investigation capabilities |  Rich visualization and reporting capabilities security analysts to efficiently investigate anomalous activities. Full meta and drill-down capabilities help you focus on risks, not noise.
| Quickly respond to threats and reduce risk |  NetWitness IoT provides enterprises with capabilities to monitor gateways, servers, and connected devices for all types of behavioral anomalies and produce focused and actionable alerts.
| Integration with multiple IoT platforms |  NetWitness IoT integrates with leading IoT management platforms and SIEMs using standard mechanisms (JSON, CEF). This type of deployment enables you to prioritize investigations and accelerate incident detection and response. In addition to NetWitness Platform you can also integrate with other security tools using standard protocols.