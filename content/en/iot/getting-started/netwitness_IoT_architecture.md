---
title: NetWitness IoT Architecture
description: NetWitness IoT architecture
weight: 924
---

## NetWitness IoT Architecture

NetWitness IoT enables analysts to get complete visibility into the whole spectrum of attacks on the entire environment.
The following diagram shows the solution architecture of NetWitness IoT:

![NetWitness IoT Sign In](/images/products/iot/IoT_architecture.png)

 In this diagram, the edge components such as the gateways, hosts, and sensors are listed. The RSA cloud service receives the data from the edge components, performs the analytics, and infers insights. The alerts and details are presented to the analysts to take action.

The collector component of the IoT Security Monitor is deployed at the Edge. Typically, it is deployed on an edge gateway or a host that runs an IoT platform for data acquisition and management based on the use cases.
The collector collects and sends the security-relevant data from the Edge to the RSA Cloud Service, which performs analytics, detects anomalies, and generates alerts.

Based on this scenario, there are the following two use cases:
+ **Use Case 1**: The first use case is focused on the plant operator who can directly log into a dashboard and see the alerts, drill down to explore the relevant context, and then take action. 
+ **Use Case 2**: The second use case is focused on the Security Operation Center (SOC). In this case, the alerts generated by IoT Security Monitor are sent to the enterprise monitoring tools, such as NetWitness Platform, which enables the SOC to extend its visibility beyond IT and into the OT side of the environment. The integration with SOC is done using Common Event Format (CEF). CEF is a log management standard that provides an interoperable way to exchange security-related information. Therefore, NetWitness IoT can be integrated with any CEF-compliant IT monitoring tool. 
NetWitness IoT integrates with leading IoT management platforms and SIEMs using standard mechanisms including JSON and CEF. This capability helps you easily add advanced security to existing deployments.

Typically, a Small-to-Medium Business (SMB) enterprise may be interested only in the first use case, whereas a large enterprise will most likely benefit from both use cases.