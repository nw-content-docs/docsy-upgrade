---
title: Investigate an Alert
description: Investigate an alert
weight: 990
---

## Investigate an Alert

You can view an alert and conduct a detailed investigation on any alert that NetWitness IoT detects.

**To investigate an alert**

1.	Log in to NetWitness IoT.
2.	Click **Alerts**.
3.	On the Alerts page, click any alert that you want to investigate.

    The Alert Rule page appears with alert details.
4.	Review the alert details.
5. To copy the alert data, move the **Raw Data** toggle switch and copy the raw data.
6. Use the raw data to conduct further investigation on the alert.

   For more information about the parameter and field descriptions of NetWitness IoT alerts, see [Parameters and field descriptions of NetWitness IoT alerts](/products/iot/investigate/parametersandfielddescriptionsofnetwitnessiot/)
