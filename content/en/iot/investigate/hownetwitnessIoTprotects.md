---
title: NetWitness IoT for Security Investigation
description: NetWitness IoT for security investigation
weight: 925
---

## NetWitness IoT for Security Investigation

NetWitness IoT protects your IoT devices by detecting the following types of threats and anomalous behaviors:

{{% table table-bordered %}}

Types of Threats | How detection works
---------|----------
 Blacklisted Process | Detects all types of blacklisted processes and alerts when a host launches a process and the executable name exactly matches any entry in the processBlacklist array.
 Known Risky Outbound Connection | Detects any known risky outbound connection and alerts you based on the threat score for the destination IP of an outbound connection and alerts if any connections are detected beyond the defined threshold.
 Anomalous New Connection Volume | Detects anomalous new connection volume and alerts you based on the number of connections from the observed baseline during the last hour.
 PCR | Detects and alerts you based on the volume of traffic leaving a host compared to the volume of traffic coming into a host and alerts when this ratio changes significantly from observed behavior over the previous day.
 Anomalous Inbound Connection External | Detects all anomalous inbound external connections and alerts you based on the changes in the source of inbound connections from the external network compared to previously observed connections.
 Anomalous Process | Detects all types of anomalous processes and alerts you based on the running processes that have not been observed for this host in the past.
 Anomalous Inbound Connection | Detects all anomalous inbound connections and alerts you based on the changes in the source of inbound connections from the local network compared to previously observed connections.
