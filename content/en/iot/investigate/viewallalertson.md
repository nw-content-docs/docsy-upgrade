---
title: View all alerts
description: View all alerts
weight: 985
---

## View all alerts

NetWitness IoT enables you to view all the alerts. You can filter the alerts and find the specific alert.

**To view all alerts**
1.	Log in to NetWitness IoT.
2. Click **Alerts**.

   The Alerts page is displayed.
   
![NetWitness IoT all alerts](/images/products/iot/view_all_alerts.png)

   You can choose rows per page and view all alerts.

For more information about the parameter and field descriptions of NetWitness IoT alerts, see [Parameters and field descriptions of NetWitness IoT alerts](/products/iot/investigate/parametersandfielddescriptionsofnetwitnessiot/)