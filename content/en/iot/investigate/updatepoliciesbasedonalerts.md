---
title: Update Policies based on Alerts
description: Update policies based on alerts
weight: 1010
---

## Update Policies based on Alerts

You can modify global policies that are used for generating alerts. You can change policies to set thresholds and generate more alerts. You can ignore some types of events or set threshold limits to control the number of alerts that are generated.

{{% notice note %}}
Updating the policies can create an impact on the way alerts are generated. Therefore, you need to be cautious while updating policies.
For more information about the parameter and field descriptions of NetWitness IoT alerts, see [Parameters and field descriptions of NetWitness IoT alerts](/products/iot/investigate/parametersandfielddescriptionsofnetwitnessiot/).
{{% /notice %}}

**To change policies for alerts**

1. Log in to NetWitness IoT.
2. Click **Policy**.
3. On the right panel, under **Editor**, modify a policy.

   The Definitions panel lists the policy parameters that you can use for tuning the policies.
5. Do one of the following:
   * To save your changes, click **SAVE**.
   * To discard your changes, click **DISCARD**.
