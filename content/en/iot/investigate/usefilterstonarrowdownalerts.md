---
title: Use Filters to narrow down Alerts for Investigation
description: Use filters to narrow down alerts for investigation
weight: 995
---

## Use filters to narrow down alerts for investigation

You can use filters to narrow down the alerts and further investigate an alert. You can use different filters to find an alert or a set of alerts and then take action.

{{% notice note %}}
Filter list is dynamic based on alerts in the system. By default, if you do not select a specific category in the filter, you can only view alerts that the system currently generates. If no alert is detected from a specific category, NetWitness IoT does not display that category in the Alert Filters window. For example, if 0 alerts are detected from ANOMALOUS_PROCESS category, then ANOMALOUS_PROCESS category will not be displayed in the Alert Filters window.
{{% /notice %}}

For more information about the parameter and field descriptions of NetWitness IoT alerts, see [Parameters and field descriptions of NetWitness IoT alerts](/products/iot/investigate/parametersandfielddescriptionsofnetwitnessiot/).

**To filter alerts**

1. Log in to NetWitness IoT.
2. Click **Alerts**.
3. On the **Alerts** page, click the **Filter** icon.
4. In the **Alerts Filters** page, select a **Policy Rule** from the following list:
   * `ANOMALOUS_PROCESS`
   * `ANOMALOUS_OUTBOUND_CONNECTION`
   * `BLACKLISTED_PROCESS`
   * `KNOWN_RISKY_INBOUND_CONNECTION`
   * `PCR`
   * `UNGRACEFUL_CONTAINER_SHUTDOWN`
   * `ANOMALOUS_OUTBOUND_CONNECTION_SITELOCAL`
   * `ANOMALOUS_INBOUND_CONNECTION_CONTAINER`
   * `ANOMALOUS_INBOUND_CONNECTION_SITELOCAL`
   * `ANOMALOUS_INBOUND_CONNECTION_EXTERNAL`
   * `ANOMALOUS_OUTBOUND_CONNECTION_BY_CLASSIFICATION`
   * `ANOMALOUS_NEW_CONNECTION_VOLUME`
5. Choose a date and time.
6. Select a severity from the list:
    * High
    * Medium
    * Low
7. Choose a country from the list.
    * Select **False Positive**.
    * Select **Viewed Alerts**.
8. Click **Close**.
