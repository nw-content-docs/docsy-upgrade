---
title: View Available Gateways
description: View available gateways
weight: 960 
---

## View Available Gateways

In NetWitness IoT, a host is also known as the gateway where the NetWitness IoT agent is installed and configured.
You can view all the gateways or hosts and learn more about the alerts generated on each gateway.
You can view details about each alert, including the date and time stamp of the alert, the severity, meta, and rule associated with the alert.

You can select a gateway, view the status and learn more about the alerts that are generated. You can click an alert, view all the details about the alert, and then take necessary action.

**To view available gateways**

1. Log in to NetWitness IoT.
2. Click the **Hosts** tab and view available Gateways.

   ![View available gateways in NetWitness IoT](/images/products/iot/available_gateways.png)

   The following table lists the parameters on the Hosts page:

{{% table table-bordered %}}

Parameters |  Description
|:--- |:--- |
|  Alerts | Number of Alerts detected on the gateway.|
| Inactive Containers |  Displays the number of inactive containers.
| Active Containers | Displays the number of active containers.
| Host OS | Provides information about the operating system of the gateway on which the agent is installed.
| Modules Loaded | If all the required modules for the agent are installed on the gateway, the value is displayed as true; otherwise, it is false.
| IP | The IP address of the Gateway on which the agent is installed.
| Conntrack Acct Enabled | If the Conntrack module installed is correct and the kernel options (`nf_conntrack_acct`, `nf_conntrack_timestamp`) are set, the value is displayed as true. If the module installed is incorrect and the flags are not set, the value is displayed as false. 
| Real-Time Process Monitoring | If the Gateway on which the agent is installed is not a CoreOS, the value is set to true; else, it is displayed as false.
| Last Updated | The timestamp at which cloud service last collected data from the agent.
| R. Created | Displays the UTC timestamp when this alert was recorded.
| Rule | Policy Rules that are violated such as Anomalous New Connection Volume, Anomalous Inbound Connection External.
| Severity | The severity of the Alert. An alert can have Low, Medium, High severity statuses.
| Meta | Provides brief information about the alert. If you click any alert, a new window is displayed with the information such as  Endpoint Category, Container, Container Process Profiles, Endpoint Category (Geo).
| Actions | Displays what action you performed for each alert: <br>• **Viewed** <br>• **False Positive**



