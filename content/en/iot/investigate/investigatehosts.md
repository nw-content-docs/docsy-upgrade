---
title: Investigate Hosts
description: Investigate Hosts
weight: 1000
---

## Investigate Hosts

The NetWitness agent is deployed on every gateway or host and collects data from all the devices that are connected to these hosts.
You can go to the Gateways and view all the alerts that were detected. You can review the details of an alert and conduct a detailed investigation. You can assign the status as viewed for each alert. If you find an alert invalid, you can mark it as a false positive.

{{% notice note %}}
To investigate, you can also get more details about an alert from the Alerts page or a Hosts page.
{{% /notice %}}

For more information about the parameter and field descriptions of NetWitness IoT alerts, see [Parameters and field descriptions of NetWitness IoT alerts](/products/iot/investigate/parametersandfielddescriptionsofnetwitnessiot/).

**To investigate hosts**

1. Log in to NetWitness IoT.
2. Click **Hosts**.
  
   Available Gateways are listed on the left panel.
3. Click a Gateway to see the alerts.
  
   You can choose different gateways and see the list of alerts detected on that gateway or host.
   
4. On the left panel, review the list of alerts that were detected on that gateway.
5. Click an alert.
   
   The **Alert Rule** window lists the details of the alert.

6. Review the alert details.
7. Toggle the **Raw Alert** and copy the strings. 
   
   You can continue to analyze and take action on these alerts.

