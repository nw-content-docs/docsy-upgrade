---
title: Investigate
linkTitle: "Investigate IoT"
description: >
   Provides information on using NetWitness IoT to identify and investigate the alerts, hosts, and take action on them.
weight: 20
---
