---
title: View Alerts on NetWitness IoT Dashboard
description: View alerts on NetWitness IoT dashboard
weight: 980
---

## View Alerts on NetWitness IoT Dashboard

NetWitness IoT provides an intuitive dashboard that helps easy investigations. The rich visualization and reporting capabilities enable efficient investigations of all types of anomalous activities. NetWitness IoT provides full meta and drill-down capabilities to enable analysts to focus on risks.

Using the NetWitness IoT Dashboard, you can:
* View alerts by severity
* View alerts for the last 30 days
* View alerts based on rules
* View alerts by country
* View alerts by region

**To view alerts on NetWitness IoT Dashboard**
1.	Log in to NetWitness IoT.
2. On the top left, click Dashboard.
   ![NetWitness IoT dashboard](/images/products/iot/view_dashboard.png)
3. View widgets on the Dashboard tab to learn more about alerts that are detected based on different categories.
4. Hover over each element and view the number of alerts detected.

{{% notice note %}}
NetWitness IoT enables you to view the number of alerts that were detected in a specific period. You can also customize and select a time period and view all the alerts that were detected in that slot.
{{% /notice %}}

For more information about the parameter and field descriptions of NetWitness IoT alerts, see [Parameters and field descriptions of NetWitness IoT alerts](/products/iot/investigate/parametersandfielddescriptionsofnetwitnessiot/)