---
title: Take Action on Alerts
description: Take Action on Alerts
weight: 1005
---

## Take Action on Alerts

You can view all the alerts detected on each gateway or host and take action on each alert. When you finish the investigation on an alert, you can mark it as Viewed. When you search for alerts, the viewed category helps filter alerts that are already marked as viewed. This helps save your time and effort in finding the proper alert and respond.

When you investigate an alert, if you notice that an alert is not valid, you can mark the alert as False Positive. When you mark an alert as false positive, you can use the filters to hide the invalid alerts and focus on the valid alerts.

For more information about the parameter and field descriptions of NetWitness IoT alerts, see [Parameters and field descriptions of NetWitness IoT alerts](/products/iot/investigate/parametersandfielddescriptionsofnetwitnessiot/).

**To identify an alert as False Positive**
1. Log in to NetWitness IoT.
2. Click **Alerts**.
   
   You can also click the Hosts tab and find alerts that are detected on different gateways.
3. Click an alert.  
   The Alert Rule window lists the details of the alert.
4. Review the alert details.
5. If you notice that an alert is legitimate, on the top right, click **False Positive**.
   
   When you mark an alert as false positive, you can use the filters to hide the alert and focus on the valid alerts.

**To mark the alert as Viewed**

1. Log in to NetWitness IoT.
2. Click **Alerts**.
   
   You can also click the Hosts tab and find alerts that are detected on different gateways. 
3. Review the alert details 
4. To mark the alert as viewed, on the top right, click **Viewed**.
   
   When you mark an alert as Viewed, you can use the filters to hide the alerts you reviewed and focus on the new alerts.
