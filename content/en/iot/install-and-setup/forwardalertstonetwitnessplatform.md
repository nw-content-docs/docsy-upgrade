---
title: Integrate with NetWitness or other SIEM platforms
description: Integrate with NetWitness or other SIEM platforms
weight: 970
---

## Integrate with NetWitness Platform or other SIEM Solutions

You can integrate the NetWitness IoT solution with NetWitness Platform or other SIEM solutions to automatically forward the alerts. Analysts can view the alerts related to IoT and review them. Analysts can identify if similar alerts are detected on other entities and understand how it impacts the environment.

{{% notice note %}}
You can create and manage different destination endpoint groups. The first added Endpoint acts as the primary alert destination, and additional endpoints within a group serve as backups.
{{% /notice %}}

**To integrate NetWitness IoT with NetWitness Platform**

1. Log in to NetWitness IoT.
2. Click the **Alert Shipper** tab and click **+ New Endpoint**.
   ![NetWitness IoT alert shipper](/images/products/iot/alert_shipper.png)
3. Click **+ New Endpoint**. The **New Endpoint** window appears.
   ![NetWitness IoT alert shipper](/images/products/iot/alert_endpoint.png)
4. Follow the on-screen instructions to add the Endpoint.
5. After you add an endpoint, enable **Alert Shipping**.
