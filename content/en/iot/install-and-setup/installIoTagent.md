---
title: Install NetWitness Agent
description: Install NetWitness agent
weight: 955
---

## Install NetWitness Agent

You need to install the NetWitness IoT agent on every host or gateway that you use to manage IoT devices. In NetWitness IoT, a host is also known as the gateway where the NetWitness IoT agent is installed and configured. If you have multiple hosts, you need to install and configure the NetWitness IoT agent on every host.
The NetWitness IoT agent image is available in the following formats:
* Docker
* Binary

{{% notice note %}} 
A beta version of the NetWitness IoT Windows agent is currently available. Contact NetWitness Customer Support and get the application details.
{{% /notice %}}

You need to install the NetWitness IoT agent on a Linux platform. To install and set up NetWitness IoT, you need to download the access key and agent on the same directory on a host.

{{% notice note %}}
The installation procedures provided here are focused on manual installation. If you belong to a large organization with multiple gateways, you can integrate the installation process with existing software deployment tools in your organization.
{{% /notice %}}

**To download and install NetWitness IoT agent using Docker**
1.	Log in to NetWitness IoT.
2.	Click **Agent**.
![NetWitness IoT agent installation](/images/products/iot/agent_install.png)
3. Download `RISM-AccessKey-XXXX.csv` into the directory where you want to set up the NetWitness IoT agent.
   For example, you can create the following directory on the host to store the agent and docker files:

   `~/RsaRISM/`
4. To download and install the docker image, click **DOCKER**, and click the tar file that suits your host:
      * `rism-agent-arm32-docker-1.0.tgz`
      * `rism-agent-arm64-docker-1.0.tgz`
      * `rism-agent-x86_64-docker-1.0.tgz`

{{% notice note %}}
Make sure that you downloaded the agent tar file and the access key on the `~/RsaRISM/` directory on the host.
{{% /notice %}}

5. Extract the docker version, use the following command:

   `tar xvzf rism-agent-*.tgz`
6. To install the Docker version, run the following command:
   
   `./run-rism-agent.sh`
7. Follow the on-screen instructions to complete the installation.

**To download and install NetWitness IoT agent using Binary**
1.	Log in to NetWitness IoT.
2.	Click **Agent**.
![NetWitness IoT agent installation](/images/products/iot/agent_install.png)
3.	Download `RISM-AccessKey-XXXX.csv` into the directory where you want to set up the NetWitness IoT agent.
For example, you can create the following directory on the host to store the agent and docker files:

 `~/RsaRISM/`
4.	Click **Agent** and click **BINARY**, and click the tar file that suits your host:
      * `rism-agent-arm-binary-1.0.tar`
      * `rism-agent-arm64-binary-1.0.tar`
      * `rism-agent-x86_64-binary-1.0.tar`

{{% notice note %}}
Make sure that you downloaded the agent tar file and the access key on the `~/RsaRISM/` directory on the host.
{{% /notice %}}

5.	Extract the file you downloaded using the following command:
 
    `tar xvzf rism-agent-*.tar`
6.	To install the Binary version, run the following command:
    
    `./run-rism-agent.sh -b`
7.	Follow the on-screen instructions to complete the installation.


**To verify successful installation**

+ 	Ensure there are no error messages in logs during NetWitness IoT agent startup.
+ 	If the installation is successful, the agent will be listed as Gateway when you log into NetWitness IoT.
