---
title: Install and setup
linkTitle: "Install and setup IoT"
description: >
 Provides information for installing and configuring the agents and troubleshoot any issues.
weight: 10
---
