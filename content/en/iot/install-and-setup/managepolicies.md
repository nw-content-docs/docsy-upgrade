---
title: Manage Policies
description: Manage policies
weight: 975
---

## Manage Policies

Policies are a set of monitoring rules targeted at all the IoT devices connected to a gateway on a network.

Using policies, you can:
* Monitor all Network traffic of IoT devices.
* Monitor all the processes associated with IoT devices.
* Detect known rule violations and unseen anomalies in IoT devices.

{{% notice note %}}
You can only modify an existing policy. You cannot create or delete a policy.
{{% /notice %}}

By default, NetWitness IoT provides a set of monitoring policies or rules automatically applied to all the IoT devices connected to the hosts. You can update a policy and define the rules for the policy. For more on the parameters and values, see [Parameters and field descriptions of NetWitness IoT alerts](/products/iot/investigate/parametersandfielddescriptionsofnetwitnessiot/).

{{% notice note %}}
You can modify global policies that are used for generating alerts. You can change policies to set thresholds and generate more alerts. You can ignore some types of events or set threshold limits to control the number of alerts that are generated. For more information, see [Update policies based on alerts](/products/iot/investigate/updatepoliciesbasedonalerts/).
{{% /notice %}}


**To manage policies**

1. Log in to NetWitness IoT.
2. Click **Policy** to open the Policy Configuration page.

   ![NetWitness IoT Policy Configuration](/images/products/iot/policy_configuration.png)
   
3. In the Editor section, modify the required policy parameters.
4.	 Click **Save**.


