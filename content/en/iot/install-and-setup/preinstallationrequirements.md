---
title: Pre-installation Requirements
description: Pre-installation requirements
weight: 950
---

## Pre-installation Requirements

The NetWitness IoT agent monitors multiple aspects of the host system. Hence, it requires privileged access to various parts of the host system. For the NetWitness IoT agent to operate optimally, the Linux kernel must be configured.
Before you install the NetWitness IoT agent, verify that the system meets the following requirements.
Configure the Linux kernel with:
+ Conntrack kernel modules 
+ Forkstat kernel modules

{{% table table-bordered %}}
Requirements |  Specifications
|:--- |:--- |
|  Supported Platforms | NetWitness IoT Agent is available as a standalone binary and docker container. Currently, NetWitness IoT Agent is supported in the following Linux platforms: <br> • amd64 <br> • arm32 <br> •	arm64|

{{% notice note %}} 
A beta version of the NetWitness IoT Windows agent is currently available. Contact NetWitness Support and get the application details.
{{% /notice %}}

## Configure Conntrack Kernel Modules

Conntrack is the name for the Linux kernel subsystem that performs connection tracking. 
You need to configure Conntrack in one of the following ways:
+ Compile into the Linux kernel 
+ Load as kernel modules 

The NetWitness IoT agent connects to this kernel service and receives notifications based on network connection changes. 

To use this service, you must make sure the following:

+ Correct kernel configuration if required.
+ NetWitness IoT agent needs to run with sufficient privileges to access this service.

In case the agent is running outside a container, it needs to run as a root.
If the Conntrack is running in a docker container, the container needs to have NET_ADMIN capability and view the hosts' network.

Turn on the following kernel options:
+ nf_conntrack_acct 
+ nf_conntrack_timestamp
The two kernel options need to be turned on to enhance the Conntrack output if the correct module is installed.

{{% notice note %}} 
If you set up the modules correctly, the kernel options will be automatically enabled when the NetWitness IoT agent starts.
{{% /notice %}}

## Configure Forkstat Kernel Modules
You need to compile the Forkstat kernel in the correct order for the Forkstart to function.
Forkstat kernel needs to be built with the `CONFIG_PROC_EVENTS`, and `CONFIG_CONNECTOR` options enabled.
It appears that if `CONFIG_CONNECTOR` is compiled as a loadable module, `CONFIG_PROC_EVENTS` will not be enabled. 

Make sure that the kernel configuration should be like the following:

`# if CONFIG_CONNECTOR=m`

*`# PROC_EVENTS will not be built`*

*_`CONFIG_CONNECTOR=y`*

*`CONFIG_PROC_EVENTS=y`*

You can view the above details in the logs when the NetWitness IoT agent starts if Conntrack and Forkstat are correctly installed and configured.
The flags in italic need to be enabled and have the values shown in Conntrack, Forkstat enabled section for agent to work fine.
The NetWitness IoT agent prints log messages during startup if any modules are not configured or available in the kernel.

Go to the Troubleshoot installation issues section to see how Conntrack and Forkstat are correctly enabled and configured.
