---
title: Release Information
linkTitle: "Release Information IoT"
description: >
  Provides information about release informations and new enhancements.
weight: 40
---

## Release Information: Introduction to NetWitness IoT

NetWitness, an RSA business (@RSAsecurity), a globally trusted partner for some of the world’s largest and most security sensitive organizations, introduces NetWitness IoT, a SaaS-native solution that delivers visibility across an organization’s critical infrastructures, including their Internet of things (IoT) and operational technology (OT) systems. NetWitness IoT provides enterprises with security monitoring for disparate IoT and OT devices at scale, by monitoring gateways, servers, and the attached devices for behavioral anomalies to produce focused and actionable alerts.

**Visibility across edge and IoT devices**

There are billions of IoT devices deployed by enterprises, including sensors, robots, cameras, meters and more. NetWitness IoT provides visibility into this important and growing component of your infrastructure.

NetWitness IoT detects threats to IoT and edge devices using advanced behavior analytics. The cloud service identifies anomalies and indicators of compromise and presents alerts in a modern user interface with rich data and response tools.

**Extending SOC visibility**

NetWitness IoT seamlessly integrates with NetWitess platform and provides a unified view for the security staff to monitor the IT and IoT environments. Additionally, NetWitness IoT integrates with leading IT security monitoring tools from other vendors so you can more easily extend the existing security operation deployments. 

**Machine learning and behavioral alerting**

The wide range of complex functions performed by IoT devices makes it hard for security teams to spot anomalous behavior. NetWitness IoT applies advanced machine learning and behavior analytics, to quickly identify instances where devices could be compromised.

**Dashboard and investigations**

Rich visualization and reporting capabilities enable everyone from operations managers to security analysts to efficiently investigate anomalous activity. Full meta and drill-down capabilities help you focus on risks, not noise.

**Integration with multiple IoT platforms** 

IoT devices and protocols are famously complex. NetWitness IoT integrates with leading IoT management platforms from other vendors so you can more easily add advanced security to existing deployments. 

**IoT Edge Ecosystem Partners**

NetWitness IoT is part of a growing ecosystem of Edge IoT leaders. These RSA Ready certified products and partners help organizations around the globe analyze, plan, design, manage, and operate IoT systems of every size and type. NetWitness IoT provides a layer of RSA-quality security monitoring, to protect these critical assets and enable valuable innovation.

