---
title: How NetWitness IoT Protects
linkTitle: "Security"
description: Secure your IoT infrastructure
weight: 30
---

## Overview

NetWitness IoT provides complete visibility into your connected IoT environment.
NetWitness IoT enables organizations to extend their Security Operation Center (SOC) visibility beyond IT and into IoT and OT (Operational Technology).
NetWitness IoT provides the ability to monitor and detect threats end-to-end. For example, an attack initiated from the IT systems and laterally moved to the OT systems can be easily detected. The analysts can get visibility into the complete spectrum of attacks on the entire environment. NetWitness IoT is part of NetWitness portfolio and seamlessly integrates with NetWitness Platform for extending SOC visibility into IoT/OT.

## How it Works?

NetWitness IoT detects threats and anomalies in your IoT infrastructure and provides the following additional capabilities:
 - Visibility across edge and IoT devices. Billions IoT devices are deployed every year, including sensors, robots, cameras, meters and more. NetWitness IoT provides visibility into this important and growing infrastructure.
 - Use of latest technology along with advanced threat intelligence to quickly identify when IoT devices are compromised or detect any anomalous behavior.
 - Rich visualization and reporting capabilities enable everyone from operations managers to security analysts to efficiently investigate anomalous activity. Full meta and drill-down capabilities help you focus on risks, not noise.
 - Integration with multiple IoT platforms. IoT devices and protocols are known for their complex behavior. NetWitness IoT integrates with leading IoT management platforms and SIEMs using standard mechanisms (JSON, CEF). This capability helps you easily add advanced security to existing deployments. In addition to NetWitness Platform you can also integrate with other security tools using standard protocols.

## IoT Edge Ecosystem Partners

NetWitness IoT is part of a growing ecosystem of Edge IoT leaders. These RSA Ready certified products and partners help organizations around the globe analyze, plan, design, manage, and operate IoT systems of every size and type. NetWitness IoT provides a layer of RSA-quality security monitoring, to protect these critical assets and enable valuable innovation.

For partner details and contact information, please visit the RSA Ready site and select NetWitness IoT.




