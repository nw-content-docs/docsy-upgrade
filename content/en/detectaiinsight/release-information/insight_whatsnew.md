---
title: What's New
description: Provides information about new features and enhancements for Detect AI Insight.
weight: 200
---
The following features and enhancements have been introduced in NetWitness Detect AI Insight:

### August 4, 2022

## Introduction to NetWitness Detect AI Insight

NetWitness Detect AI Insight is a SaaS solution available as an extension for a NetWitness Network, Detection & Response (NDR). Detect AI Insight is an advanced analytics solution that leverages unsupervised machine learning to empower the response of the Security Operations Center (SOC) team. Detect AI Insight continuously examines network data collected by the Decoder to discover, profile, categorize, characterize, prioritize, and track all assets.

NetWitness Detect AI Insight identifies the assets in the enterprise to alert analysts of their presence. The discovered assets are automatically categorized into groups of similar servers and prioritized based on their network profiles. These assets are presented to analysts to guide them to focus on certain assets to protect their organization.

This helps organizations to:

+ Asset discovery and characterization.
+ Monitor critical Assets.
+ Leverage the security operations team to triage based on prioritization.
