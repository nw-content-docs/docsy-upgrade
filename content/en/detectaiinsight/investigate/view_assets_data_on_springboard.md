---
title: View Assets Data on Springboard
description: This topic provides information about viewing Assets data on the Springboard panel and do further analysis and investigation.
weight: 180
---

## View Assets Data on Springboard

NetWitness Platform XDR Springboard provides analysts with visibility into all the behavior details of an asset in the form of asset data. These behavior details are based on cloud analytics performed by Detect AI Insight on uploaded network metadata by the Insight sensor. 

The Springboard retrieves Detect AI Insight data results from the cloud leveraging the Cloud Connector sensor. With this data, analysts can create a new panel with data type **Assets** to monitor and identify risky assets in their environment, which helps in further investigation of an asset.

{{% notice note %}}
By default, the new asset Springboard panel will not appear in Springboard and you need to add the assets panel. 
{{% /notice %}}

The assets IP are categorized based on the Exposure Rank. The critical ones are highlighted with red and with rank 100 is the maximum, which requires immediate attention.

   <div>
      <img src="/images/products/detectai/assets_panel1.png" alt="Assets view tab" width="910" height="min">
   </div>
   <br>
Analysts can perform the following actions on the Assets Panel:

+ Click an asset IP in the assets panel to view or investigate the assets details with relevant filters applied in the **Investigate > Events** view.

+ Click <img src="/images/products/detectai/icons/arrowright.png" alt="Right Arrow" class="inline"/> at the top of the assets panel to view all the results in the **Investigate** > **Events** view.

+ Click <img src="/images/products/detectai/icons/edit_icon.png" alt="Edit Icon" class="inline"/> to edit the asset panel details.

+ Click <img src="/images/products/detectai/icons/refresh_icon.png" alt="Refresh Icon" class="inline"/> to refresh and load the latest data into the panel.

**To add the Assets panel**

1. Log in to the NetWitness Platform XDR.

2. Click <img src="/images/products/detectai/icons/addpanel.png" alt="Add panel" class="inline"/> either on the top or on the right side of the view or click <img src="/images/products/detectai/icons/ic_addpanel.png" alt="Add panel" class="inline"/> at the bottom of the view to add a panel.

   The Create New Panel dialog is displayed. The following figure is an example of the Assets panel configuration.
   ![create assets panel](/images/products/detectai/create_assets_panel.png)

3. In the **Input Settings** Section: 

     * **Name:** Enter a unique name for the panel. The name can include letters, numbers, spaces, and special characters, such as _ - ( ) [ ].

     * **Number of Results:** By default, the number of results is 25. Specify the number of results that range from 25 to 100.

     * **Data Type:** Select the data type as **Assets**.

     * **Data Source:** Select the source of the data to use for the panel. You can use Broker or Concentrator to filter the query results on the **Investigate** > **Events** page for further investigation.

4. In the **Output Settings** section, select the appropriate settings based on the data type. 

5. Click **Add Panel**.

6. Click **Save Board** once you have added all the panels.

   For more information, see [Managing the Springboard](https://community.netwitness.com/t5/netwitness-platform-online/managing-the-springboard/ta-p/668905).