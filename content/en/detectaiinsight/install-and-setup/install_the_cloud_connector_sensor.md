---
title: Install the Cloud Connector Sensor
description: This topic provides information about how to install the Cloud Connector Sensor.
weight: 140
---

## Install the Cloud Connector Sensor

Cloud Connector Sensor is a new on-premises service that is installed on the Admin Server and registers as a sensor that provides a gateway to fetch the data from the NetWitness Cloud and transfer the data to the on-premises NetWitness Platform XDR for further analysis and investigation.

**Prerequisites** 

 Ensure that the NetWitness Platform XDR and the host (Admin Server) are on version 12.2 or later.

{{% notice note %}}
Every customer needs to install only one Cloud Connector Sensor in their environment.
{{% /notice %}}

**Step 1: Install the Cloud Connector Sensor**

1. Log in to the NetWitness Platform XDR as an administrator and go to <img src="/images/products/detectai/icons/adminicon.png" alt="admin icon" class="inline"/> **Admin** > **Hosts**.
 
   The Hosts view is displayed.
2. Select the host (Admin Server) and click <img src="/images/products/detectai/icons/installhst1.PNG" alt="install button" class="inline"/>.

   A dialog listing all the services already installed on this host is displayed and seeks your confirmation if you want to install a new service.

3. Click **Yes**.

   The Install Services dialog is displayed.

4. Select the **Cloud Connector Service** from the **Category** drop-down menu, and click **Install**.
   <div>
      <img src="/images/products/detectai/Cloudconnector_install.png" alt="insight tab" width="910" height="min">
   </div>
   <br>
5. Go to <img src="/images/products/detectai/icons/adminicon.png" alt="admin icon" class="inline"/> **Admin > Services** to verify successful Cloud Connector Service installation.

**Step 2: Download the activation package**

1. Log in to the NetWitness Detect AI.

2. Go to <img src="/images/products/detectai/icons/admin3.png" alt="admin icon" class="inline"/> **Admin** > **Sensors** > **Sensor Downloads**.

3. Click the **Cloud Connector** tab.
   <div>
      <img src="/images/products/detectai/cloud-connector.png" alt="insight tab" width="910" height="min">
   </div>
4. Under **Activation Package**, click <img src="/images/products/detectai/icons/download_activation_package2.png" alt="download icon" class="inline"/> to download the activation package. 

**Step 3: Register the Sensor**

You need to copy the Activation Package to the Cloud Connector sensor directory to complete the registration of the sensor. 

{{% notice note %}}
+ The same activation package can be used to register multiple sensors.
+ Ensure you use the most recently downloaded activation package. 
+ If the activation package is not available, generate a new one.
+ The activation package contains sensitive information, you must handle it carefully and don’t share it with anyone.
{{% /notice %}}

1. SSH to the host on which the Cloud Connector Sensor is installed.
2. Copy the {{% highlightinfo %}}device-activation-package.json{{% /highlightinfo %}} file downloaded from the NetWitness Detect AI to the {{% highlightinfo %}}/var/lib/netwitness/cloud-connector-server directory{{% /highlightinfo %}} on the Cloud Connector Service host.
3. Navigate to the following directory, by running the command:

   {{% copycode %}}
   <code>
   cd /var/lib/netwitness/cloud-connector-server</code>
   {{% /copycode %}}
    

4. Change the user and group of the {{% highlightinfo %}}device-activation-package.json{{% /highlightinfo %}} file to {{% highlightinfo %}}netwitness{{% /highlightinfo %}} by executing the following command:

   <div class="highlight">
     <pre class="chroma" style="padding-top:1rem; border: solid; border-width: thin; background-color: #F8F9FA";>
       <code>chown netwitness:netwitness device-activation-package.json</code>
     </pre>
   </div>

5. To verify if Cloud Connector Sensor is connected successfully, log in to the NetWitness Detect AI user interface, and go to **Sensor List** and check if the sensor appears as **CLOUD_CONNECTOR** with connected status.





