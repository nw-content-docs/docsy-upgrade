---
title: Install and setup
linkTitle: "Install and setup"
description: Provides information for installing and configuring the sensors. It also provides information about deleting sensors.
weight: 20
---
