---
title: Locate the Device ID for a Specific Insight Sensor
description: This topic provides information about how to locate the device ID for a specific Insight Sensor.
weight: 170
---

## Locate the Device ID for a Specific Insight Sensor

In case of multiple Detect AI Insight Sensor deployments, you will require the device ID if you want to delete or check the status of a specific Insight Sensor. You need to open the Insight Sensor host (Packet Decoder) on the Services page and find the device ID in Explore view on the NetWitness Platform user interface.

**To locate the device ID for a specific Insight Sensor**

1. Log in to the NetWitness Platform XDR.
2. Go to <img src="/images/products/detectai/icons/adminicon.png" alt="admin icon" class="inline"/> **Admin** > **Services**.
3. In the **Services** list, search Packet Decoder in the **Filter** field. 
4. Select the Packet Decoder and click <img src="/images/products/detectai/icons/actions_button.png" alt="admin icon" class="inline"/> > **View** > **Explore**.
   
   The explore view for the service is displayed.
   <div>
      <img src="/images/products/detectai/sys_view.png" alt="System view" width="890" height="min">
   </div>
5. On the left panel, click **+sys** > **stats**. The UUID and other information are displayed on the right panel.

   The UUID value contains the complete 36-characters service ID of the Packet Decoder service.

   The last 12-characters of the UUID are the Device ID of that Insight sensor.
   For example, if the UUID of the Packet Decoder service is **9b7ea15f-a823-4e44-8a4e-d397111f9c4e**, the device ID of the sensor is **d397111f9c4e**.
   <div>
   <img src="/images/products/detectai/stats_view.png" alt="Stats view" width="890" height="min">
   </div>