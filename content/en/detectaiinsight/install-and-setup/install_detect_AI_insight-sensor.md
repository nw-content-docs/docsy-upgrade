---
title: Install Detect AI Insight Sensor
description: This topic provides information about how to install Detect AI Insight Sensor on Packet Decoder.
weight: 130
---

## Install Detect AI Insight Sensor

You must install the NetWitness Detect AI Insight Sensor on every Packet Decoder host to capture the network metadata and forward them to the NetWitness Cloud. If you have multiple hosts, you need to install and configure the Insight sensor on every Packet Decoder host. 

**Supported Hosts** 

* **Packet Decoder**
* **Packet Hybrid**

**Prerequisities**

Ensure that the NetWitness Platform XDR and the host (Packet Decoder) are on version 12.2 or later.

{{% notice note %}}
You need a separate Insight Sensor to be installed for each Packet Decoder host.
{{% /notice %}}

**Step 1: Install the Detect AI Insight Sensor on Packet Decoder** 

1. Log in to the NetWitness Platform XDR as an administrator and go to <img src="/images/products/detectai/icons/adminicon.png" alt="admin icon" class="inline"/> **Admin** > **Hosts**.
 
   The Hosts view is displayed.
2. Select the Packet Hybrid and click <img src="/images/products/detectai/icons/installhst1.PNG" alt="install button" class="inline"/>.

   A dialog listing all the services already installed on this host is displayed and seeks your confirmation if you want to install a new service.

3. Click **Yes**.

   The Install Services dialog is displayed.

4. Select the **Detect AI Insight** from the **Category** drop-down menu, and click **Install**.
   <div>
      <img src="/images/products/detectai/DetectAI_insight_installation.png" alt="insight tab" width="910" height="min">
   </div>
   <br>
5. Go to <img src="/images/products/detectai/icons/adminicon.png" alt="admin icon" class="inline"/> **Admin > Services** to verify successful Detect AI Insight installation.

**Step 2: Download the activation package**

1. Log in to the NetWitness Detect AI.

2. Go to <img src="/images/products/detectai/icons/admin3.png" alt="admin icon" class="inline"/> **Admin** > **Sensors** > **Sensor Downloads**.

3. Click the **Insight** tab.
   <div>
      <img src="/images/products/detectai/cloud-connector.png" alt="insight tab" width="910" height="min">
   </div>
   <br>
4. Under **Activation Package**, click <img src="/images/products/detectai/icons/download_activation_package2.png" alt="download icon" class="inline"/> to download the activation package. 

**Step 3: Register the Detect AI Insight Sensor**

1. SSH to the Packet Decoder Host. 
2. Copy the {{% highlightinfo %}}device-activation-package.json{{% /highlightinfo %}} file downloaded from the NetWitness Platform to the {{% highlightinfo %}}/etc/netwitness/ng{{% /highlightinfo %}} directory on the Packet Decoder host.
3. Navigate to the following directory, by running the command:
   <div class="highlight">
      <pre class="chroma" style="padding-top:1rem; border: solid; border-width: thin; background-color: #F8F9FA";>
        <code>cd /etc/netwitness/ng</code>
      </pre>
   </div>
   
4. Change the user and group of the {{% highlightinfo %}}device-activation-package.json{{% /highlightinfo %}} file to {{% highlightinfo %}}netwitness{{% /highlightinfo %}} by executing the following command:
   <div class="highlight">
     <pre class="chroma" style="padding-top:1rem; border: solid; border-width: thin; background-color: #F8F9FA";>
       <code>chown netwitness:netwitness device-activation-package.json</code>
     </pre>
    </div>
10. Copy the downloaded Detect AI Insight sensor RPM package to the {{% highlightinfo %}}/root{{% /highlightinfo %}} folder on the Packet Decoder host.

{{% notice note %}}
Ensure that the install command is executed in the same folder as the RPM package.
{{% /notice %}}

5. Install the Detect AI Insight Sensor by running the following command:

    <div class="highlight">
      <pre class="chroma" style="padding-top:1rem; border: solid; border-width: thin; background-color: #F8F9FA";>
        <code>rpm -Uvh (filename of the DetectAI-Insight-RPM)</code>
      </pre>
     </div>

    Replace the {{% highlightinfo %}}filename of the DetectAI-Insight RPM{{% /highlightinfo %}} with the actual name of the RPM. <br>

6. To verify if the Detect AI Insight Sensor is installed successfully, log in to the NetWitness Detect AI user interface, and go to **Sensor List** and check if the sensor appears as **NetWork** with registered status. 
7. Login to the NetWitness Platform XDR. 
8. Navigate to <img src="/images/products/detectai/icons/adminicon.png" alt="admin icon" class="inline"/> **Admin** > select the service > **View** > **System** page and check if the Decoder is capturing the data. 
 + If the Decoder is capturing the data, stop the data capture and restart the data capture on the Decoder to start transferring the data to the cloud.
 + If Decoder was not capturing data, you must start data capture if you want data collected by this decoder to be part of Detect AI Insight. 

{{% notice important %}}
Asset information is collected throughout the day and uploaded once every 24 hours.
{{% /notice %}}