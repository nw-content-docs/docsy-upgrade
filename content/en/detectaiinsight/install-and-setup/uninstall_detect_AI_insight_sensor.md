---
title: Uninstall Detect AI Insight Sensor
description: This topic provides information about how to uninstall Detect AI Insight Sensor.
weight: 150
---

## Uninstall Detect AI Insight Sensor

If you have Detect AI Insight Sensor installed and no longer want to use it, perform the following steps to uninstall it.

{{% notice note %}}
To uninstall the Insight Sensor you require the device ID. You can find the device ID in the Explore view on the NetWitness Platform user interface. For more information, see [Locate the Device ID for a specific Insight sensor](/products/netwitnessdetectai/detectai-insight/install-and-setup/locate_the_device_id_for_a_specific_insight_sensor).
{{% /notice %}}

**To uninstall the Detect AI Insight Sensor** 

1. Log in to the NetWitness Platform XDR.

2. Navigate to <img src="/images/products/detectai/icons/adminicon.png" alt="admin icon" class="inline"/> **Admin** > **Services**.

3. In the **Services** list, select the Packet Decoder service on which the Detect AI Insight sensor is installed and click <img src="/images/products/detectai/icons/actions_button.png" alt="Admin icon" class="inline"/> > **View** > **System**.
    
    The System view for the Packet Decoder service is displayed. 

4. Click **Stop Capture**.

5. Log in to the NetWitness Platform on the Cloud.

6. Go to <img src="/images/products/detectai/icons/admin3.png" alt="admin icon" class="inline"/> **Admin** > **Sensors** > **Sensor List**.

7. Select the Insight Sensor that you want to delete using the Device ID and click **Remove Sensor**.

8. SSH to the Packet Decoder host.

9. Run the following command to uninstall:

   <div class="highlight">
     <pre class="chroma" style="padding-top:1rem; border: solid; border-width: thin; background-color: #F8F9FA";>
       <code>rpm -ev (rpm package name)</code>
     </pre>
   </div>
   Replace with the actual rpm package name.

10. (Optional) Click **Start Capture** to restart the packet capture from the Packet Decoder.

