---
title: Uninstall Cloud Connector Sensor
description: This topic provides information about how to uninstall Cloud Connector Sensor.
weight: 160
---

## Uninstall Cloud Connector Sensor

If you have a Cloud Connector Sensor installed and you no longer want to use it, perform the following steps to delete the Cloud Connector Sensor.

To delete the Cloud Connector Sensor, you must first remove the Cloud Connector Sensor from the NetWitness Detect AI and then uninstall the Cloud Connector Sensor on the NetWitness Platform XDR.

**Step 1:** Remove the Cloud Connector Sensor from the NetWitness Detect AI

1. Log in to the NetWitness Detect AI.

2. Go to <img src="/images/products/detectai/icons/admin3.png" alt="Admin icon" class="inline"/> **Admin** > **Sensors** > **Sensor List**.

3. Select the Cloud Connector Sensor you want to delete and click **Remove Sensor**.

**Step 2:** Uninstall the Cloud Connector Sensor on the NetWitness Platform XDR

1. SSH to the host on which the Cloud Connector Sensor is installed.

2. Execute the following command: 
   
   <div class="highlight">
     <pre class="chroma" style="padding-top:1rem; border: solid; border-width: thin; background-color: #F8F9FA";>
       <code>/var/lib/netwitness/cloud-connector-server/nwtools/uninstall-cloud-connector.sh</code>
     </pre>
   </div>

3. Log in to the NetWitness Platform XDR and go to <img src="/images/products/detectai/icons/adminicon.png" alt="admin icon" class="inline"/> **Admin** > **Services** to verify if the Cloud Connector Sensor is removed. 