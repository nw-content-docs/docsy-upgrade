---
title: About Detect AI Insight licenses
description: This topic provides information about Detect AI Insight licenses.
weight: 100
---

## About Detect AI Insight licenses

NetWitness Detect AI Insight licenses are valid for the time period associated with the license purchase. NetWitness Detect AI Insight provides a customer-focused licensing strategy.

The following pricing is annual and can be billed monthly:

{{% table table-bordered %}}

| Product |  Price  |  Unit  | 
|:--- |:--- |:--- |
|  NetWitness Detect AI Insight (SaaS only) | $ |  | 
