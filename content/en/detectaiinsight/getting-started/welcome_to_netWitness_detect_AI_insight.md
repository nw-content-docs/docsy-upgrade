---
title: Welcome to NetWitness Detect AI Insight
description: This topic provides an overview about Detect AI Insight.
weight: 70
---

## Welcome to NetWitness Detect AI Insight

NetWitness Detect AI Insight is a SaaS solution available as an extension for a NetWitness Network, Detection & Response (NDR) customer. Detect AI Insight is an advanced analytics solution that leverages unsupervised machine learning to empower the response of the Security Operations Center (SOC) team. Detect AI Insight continuously examines network data collected by the Decoder to discover, profile, categorize, characterize, prioritize, and track all assets actively.

NetWitness Detect AI Insight passively identifies all assets in the environment and alerts analysts of their presence. The discovered assets are automatically categorized into groups of similar servers and prioritized based on their network profiles. These assets are presented to analysts to guide them to focus on certain assets to protect their organization.

Detect AI Insight enables you to do the following:

+ Asset discovery and characterization.
+ Monitor critical Assets.
+ Leverage the security operations team to triage based on prioritization.