---
title: Log in to your Account
description: This topic provides information about how you log in to your account.
weight: 110
---

## Log in to your Account

The NetWitness Detect AI user interface provides administrators with the capability to manage and monitor Detect AI Insight services for their account. 

**Prerequisites**

Before you log on to the NetWitness Detect AI UI, ensure that you have received an email from NetWitness Detect AI containing the account URL link. 

**To Log in to NetWitness Detect AI**

1. Click on the URL provided in the NetWitness Detect AI welcome email. 

   The NetWitness Detect AI home page is displayed.
   <div>
      <img src="/images/products/detectai/login_detectai.png" alt="Login page" width="890" height="min">
   </div>
   <br>
2. Enter your registered email ID and the temporary password in the respective fields. As this is your first login, the page prompts you to reset your password.

3. Enter the new password, and confirm the same. Review the password format rules and ensure that your new password conforms to the indicated format rules

4. Click **Sign In** 

For more information on managing the administrators and enabling MFA, see [Setup and manage administrators](/products/netwitnessdetectai/detectai/getting-started/manage_administrators).