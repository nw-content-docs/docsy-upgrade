---
title: Detect AI Insight Use Cases
description: This topic provides information about Detect AI Insight use cases.
weight: 80
---

## Detect AI Insight Use Cases

Detect AI Insight provides advanced analytics capabilities to alert organizations about risky and anomalous assets.

Analysts must scan through billions of network sessions and IP addresses to protect their organization, searching for threats and anomalies. This is where NetWitness Detect AI Insight passively identifies the assets in the enterprise to alert analysts of their presence. The discovered assets are automatically categorized into groups of similar servers and prioritized based on their network profiles. These assets are presented to analysts to guide them to focus on specific assets to protect their organization.

### Use Cases

The following are typical use cases for Detect AI Insight:

* Provides asset discovery and characterization.
* Provides asset exposure rank.
* Provides efficiency in security operations through triage based on prioritization.