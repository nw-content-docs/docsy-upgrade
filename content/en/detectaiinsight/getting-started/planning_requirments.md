---
title: Planning Requirements
description: This topic provides information about system requirements and various prerequisites.
weight: 120
---

## Planning Requirements

Before you install the sensors, you must plan for the following:

+ The NetWitness Platform XDR (Admin Server and Packet Decoder Host) is on version 12.2 or later.
+ Ensure you have the administrator access to the NetWitness Detect AI user interface.
+ The host on which the Detect AI Insight Sensor and Cloud Connector Sensor will be installed needs to be connected to Amazon Web Services(AWS). This might require changes to your existing firewall rules. Hosts will need to connect to the IP ranges for the chosen deployment region. For more information on the current list of AWS IPs by region, see [AWS IP address ranges](https://docs.aws.amazon.com/general/latest/gr/aws-ip-ranges.html).
+ Ensure that the analysts have write (manage) access to create the Springboard panel. For more information, see the Springboard section in the [Role Permissions](https://community.netwitness.com/t5/netwitness-platform-online/role-permissions/ta-p/669656) topic in the *System Security and User Management Guide*.
+  Ensure that the system clock is accurate. To fix the system clock, configure the NTP server on the Admin server. For more information on how to configure NTP server, see [Configure NTP Servers](https://community.netwitness.com/t5/netwitness-platform-online/configure-ntp-servers/ta-p/669729).
+ Customers must have NetWitness Cloud tenant account with entitlements that allow Detect AI Insight capability.  

{{% notice note %}}
While performing the failover on Admin Server, the Cloud Connector Server is found inactive. In this scenario, you must uninstall and re-install the Cloud Connector Sensor to work correctly.
{{% /notice %}}

You can install Detect AI Insight Sensor on the following hosts:

{{% table table-bordered %}}

| Model | Category |
| :--- | :--- |
| S5/S6/S6E/Virtual | Packet Decoder <br> Packet Hybrid |