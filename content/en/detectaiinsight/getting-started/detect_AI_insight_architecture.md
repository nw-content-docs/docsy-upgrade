---
title: Detect AI Insight Architecture
description: This topic provides information about Detect AI Insight architecture.
weight: 90
---

## Detect AI Insight Architecture

NetWitness Detect AI Insight enables analysts to get complete visibility into unknown assets and can help increase the visibility of the assets within the organization.

<div>
  <img src="/images/products/detectai/workflow_insight1.png" alt="insight" width="950" height="min">
</div>
<br>

Detect AI Insight uses custom machine learning to process the data. The Insight Sensor collects the network metadata from the Packet Decoder and transfers metadata to the NetWitness Cloud once every 24 hours. The NetWitness Cloud merges all the network metadata received from different Insight sensors in a customer environment and provides a unified view of their network to the analysts for analysis in the Springboard assets panel view. 

The Springboard assets panel queries the Cloud Connector Sensor for asset data. Cloud Connector Sensor retrieves asset data from the NetWitness Cloud and transfers it to Springboard. This helps analysts to drill down the assets data for further investigation and take immediate action.

Detect AI Insight uses unsupervised learning techniques applied to traffic associated with the assets to determine the type and significance of the asset. The services, clients, and external clients are the parameters aggregated to determine the total traffic of an asset. Detect AI Insight also computes custom importance ranks reflecting asset exposure and activity ranks so that security teams can use them to prioritize and triage incidents.