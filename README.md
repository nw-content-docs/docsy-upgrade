# nw-docsy-upgrade

Notes on upgrade to Docsy v0.60 and various fixes to restore Hugo/Docsy best practices and normal site organization.

## Keeping the Docsy theme updateable for our team

Docsy, like other Hugo themes, can be overridden and modified to suit our needs; however, the more we modify the more we have to adjust and merge every time we update the core, OOTB template.  This can be doable, such as the [Kubernetes](https://kubernetes.io/) example, but with only a couple of Engineers dedicated part-time we don't necessarily have the resources to maintain a high level of customization.

The benefit to us for using this theme (maintained by Google) is that we have a solid, full docsite template with minimal effort to maintain on our part.  Wherever possible, we should try to use the OOTB Docsy theme templates rather than overriding/customizing them using the `/layouts/` folder.  Of course, there will be customizations that we can't live without, but we should try to minimize.

Later down the road, once we have established more history and gotten more experience with Hugo, we can always add more customization.  For now, I think we should keep it as simple as possible and get the primary work done of getting all the documentation we want completed.

### Removing the custom Dark Mode

As cool as the dark mode toggle is, there are some significant customizations to the frame of the OOTB templates to make this work.  There is also quite a bit more work to do in order to get the dark mode to work perfectly.  For instance, the MITRE matrix does not work correctly in dark mode.  I propose that we remove this for now.  We can look at re-adding it later.

In fact, nowadays there are browser extensions which can accomplish dark mode pretty well for most websites.  It seems to work pretty well with Docsy!  For now, if users want dark mode they can leverage this.  It is more important that we get our content working well.

## Review of Hugo template lookup order

Hugo first looks for a matching `type` for the current page

- If content has a `type` set in its front-matter then it will use that
- Otherwise hugo will look for the directory it is in and find a matching directory under `/layouts`
- Otherwise hugo will apply the `/layouts/_default` templates wherever there is a match
- The full search order can be found in [Hugo Docs](https://gohugo.io/templates/lookup-order/)

If there is a `baseof.html` template in this path then Hugo will use the [base-and-block](https://gohugo.io/templates/base/) template style, which is a modular approach

## Best practice tips for managing templates

Don't create customized templates if you don't have to

- Use the templates in the Docsy theme unless you really need to modify something in the template

Use the cascade to set the `type` property to `docs` to apply the style to the entire directory

- You can force sections of your documents (regardless of their directory path) to use OOTB templates (the `docs` templates for instance) by using the [cascade](https://gohugo.io/content-management/front-matter/#front-matter-cascade) setting within your `/content/_index.html` file
- When you apply this to a root folder, it applies to all files underneath without the need to set the front-matter in each of those files
- See the file `/content/en/_index.html` for an example
- This allows you to have ONE (or few) template files under `/layouts` and apply multiple files under multiple folders to the same template

If you are using the "baseof" templating approach, then you only need to modify the one file.  You don't need to copy the entire directoy of files to your local `/layouts` folder

- For instance, if you just want to add a custom javascript file in the `<header>` section then you would just need a custom `baseof.html`.  You don't need to add the `single.html`, `list.html`, etc.

## Template (/layouts) Modifications...so far...

Besides the custom templates I added to `shortcodes` and `partials`, there were a couple of template files I took from the Docsy themes default (for v0.6.0) and customized.  I added a `/layouts/docs/` folder and added a `baseof.html` and `single.html` to add the custom pagination navigation that was added to the original hugo site.  (The HTML below was previously in the `/layouts/_default/single.html` file.)

`/layouts/docs/baseof.html`

```go
    {{ block "pagination" . }}{{ end }}
```

`/layouts/docs/single.html`

```html
{{ define "pagination" }}
<style>
  button.previous-next {
    border-style: double ;
    width: 100%; 
    border-width: thin; 
    border-radius: 5px; 
    padding-left: 1rem; 
    padding-right: 1rem;
    padding-top: 1rem; 
    padding-bottom: 1rem; 
    background: white;
  }
  button.previous-next:hover {
  border-color: red;
  }
  div.pagination-nav {
    color: blue;
  }

  body.dark-mode div.pagination-nav {
    color: #6cb3e2;
  }
  
  body.dark-mode span {
    color: white;
  }

</style>

<div class="container" style="padding-top: 1rem; padding-left: 0rem; padding-right: 0rem;">
  <div class="row">
    {{if .NextInSection}}
    <div class="col" style="max-width: 48%;">
    <a class="pagination-nav__link pagination-nav__link--prev" href = {{.NextInSection.Permalink}} style="text-align: left;">
      <button type="button" class="previous-next">
        <div class="row" style="padding-right: 1rem;">
          <div class="col" style="align-items: center; display: flex;">
              <i class="fa fa-angle-double-left fa-lg" aria-hidden="true">  </i>
          </div>
          <div class="flex-col" style="text-align: right;">
                <span>Previous</span>
                    <div class="pagination-nav" style="font-weight: bold;">
                      {{.NextInSection.Name}}
                    </div>
          </div>
        </div>
      </button>
    </a>
  </div>
    {{end}}
    {{if .PrevInSection}}
    <div class="col" style="max-width: 50%;">
    <a class="pagination-nav__link pagination-nav__link--prev" href = {{.PrevInSection.Permalink}} style="text-align: right;">
      <button type="button" class="previous-next">
        <div class="row" style="padding-left: 1rem;">
          <div class="col" style="text-align: left;">
              <span>Next</span>
                  <div class="pagination-nav" style="font-weight: bold;">
                    {{.PrevInSection.Name}}
                  </div>
          </div>  
          <div class="col-auto" style="display: flex; align-items: center; text-align: right;">
            <i class="fa fa-angle-double-right fa-lg" aria-hidden="true">  </i>
          </div>   
        </div>
      </button>
    </a>
  </div>
      {{end}}
  </div>
</div>
{{ end }}
```

## Guidelines on separating content from templates

In general, we want to be able to give the content writer the freedom to modify what shows up in the site while preventing them from ever needing to look in the `/layouts` folder.  That is for the Engineers to deal with.

Content writer should not have to look at any files that have any kind of extensive use of HTML.

Use of shortcodes can help the content writers to include pieces of HTML, javascript, GO or other types of content while abstracting the code from them.

## Styling and CSS

There were a number of local `style` settings made directly to templates.  In the traditional sense, this is always discouraged due to the "decentralized" nature of styling in this manner.  For Hugo, it sometimes acceptable due to the fact that many pages are generated based off of templates. In our case, it was a little too much and the majority of the syling needed to be brought into CSS.

The Docsy template uses SASS (or `.scss` files) to dynamically complile CSS sheets at build-time.  Much of this styling is based on the Open Source **bootstrap** CSS framework.  The custom parts of CSS styling for Docsy can be found under the `/theme/docsy/assets/scss/` folder.  While it is possible to copy and override these `.scss` files directly, we can instead use the `/assets/scss/_variables_project.scss` as a way to load our own CSS to override CSS classes in a partial manner.

Notice in the `/assets/scss/_variables_project.scss` file, I have made some modifications.  Most importantly, I've added an `@import` command:

```css
/* This file is provided by Docsy as an entry point to styling.
Add styles or override variables from the theme here. */

$primary: #900c08 !default;
$primary-light: lighten($primary, 75%) !default;
$secondary: #007bff !default;
$success: #00fc15 !default;
$info: #19e4e4 !default;
$warning: #ffeb3b !default;
$danger: #ED6A5A !default;
$white: #fff !default;
$light: #D3F3EE !default;

@import "custom";

```

This allows me to set style parameters for particular classes (or even at the element level) without "completely" overriding the CSS applied by the theme.  By using the `!important` parameter, I can override a single theme setting, even if it has already been set at a higher scope:

```css
a.nav-link {
  color: $navlink !important;
}

a.nav-link:hover {
  color: $navlink-hover !important;
}
```

This provides a cleaner, more centralized approach to view/manage our custom styles.  I've still used in-line styles at times, when appropriate, but this approach should be used primarily if at all possible.

## Home page

The homepage had its content partially hardcoded using a custom `/layouts/index.html`.  The layout of the cards was controlled by iterating through lists of each folder in the content area.

- This takes away some of the freedom of the content writers to modify the home page
- Creates a lot of extra custom partial templates that we have to review/maintain as we update Docsy templates or make changes thematically to the site's style

Moving back to the Docsy approach, using `/layouts/shortcodes/blocks`

- Created custom `card` and `card-deck` as well as `external-links` and `link` and `banner` shortcodes
- Updated the `/content/_index.html` to include references to the shortcodes (to control the homepage layout)
- Removed the `/layouts/index.html`
- Removed the custom homepage templates under `/layouts/partials` (i.e. content-main.html, welcome.html, etc.)

> This has the effect of allowing the home page to be modified manually (by the content managers if needed).

Regarding the width of the content on the main page: I personally am ok with using 100% width, but I realize that we don't have as much content to show on our home page at the moment.  Once this changes then we could go ahead and expand the body of the home page to 100%.  For now, though I have modified the styling of the home page body to be controlled in the custom `/layouts/home/baseof.html` template.

- Once we do away with this width and go 100% then we can probably delete this `baseof.html` file.

The height of the "Product Documentation" banner seemed WAY too big to me.  I shortented its height, but this can still be controlled using the `/layouts/shortcodes/banner.html` shortcode.

### The Logo

Since the logo was created as an SVG, we can resize it however we like without losing any image quality.  I've added this to the `_custom.scss` file as the `.td-navbar` > `.navbar-brand` > `svg` class.

- The aspect ratio of the SVG has been locked at the file level by removing the `width` parameter
- To increase/decrease the size of the logo, simply modify the `height` parameter within the CSS

I have also reduced the size of the Logo.  This is only my suggestion, but I feel that the logo was WAY too big on our site and it made the navbar too tall, taking up too much real-estate.

## The Navbar

I think that the navbar having restricted width on the homepage, but being full-width everywhere else is wierd and unnecessary.  It restricts the real estate we have for the main nav-links and the search bar.  It is also inconsistent with the rest of the site's pages.  I went ahead and made this consistent throughout the site

I also restored the original search bar and put it in its original spot.  This seemed like an unnecessary customization to me.  In keeping with the goal of adhering to the OOTB template wherever possible this made more sense to me.

- We can still customize the search results to point the user to the community site if they don't find what they need here.
- Since we have more real-estate now in the navbar, the custom search box seemed less important

The main navigation bar was not functioning like it should.  This was due to some inconsistent setting of `menu.main` property throughout the site.  Menus can be defined by how you organize your content into folders, but they can ALSO be defined manually using the site `menu.main` parameter.  If you look at the [Hugo docs](https://gohugo.io/content-management/menus/#add-non-content-entries-to-a-menu) on this you can see how this works.  There is also a set of [menu parameters](https://gohugo.io/variables/menus/#menu-entry-variables) including the `parent` parameter, which can be used to manually configure the menu.

- The OOTB `/theme/docsy/layouts/partials/navbar.html` does NOT include any code for enabling drop-down menus in the site's main menu.  So I have customized this to enable the feature.
- There were `menu.main` settings in the front-matter of a number of `_index.md` files within the content, which didn't need to be there.  The entire menu can be set in one place, in the `/config.yaml` file.  For instance:

```yaml
menu:
# Add top-level menu items (particularly for drop-down "parent" items)
  main:
    - identifier: products
      name: Products
      weight: 20
    - identifier: detectai
      name: DetectAI
      parent: products
      url: /detectai/
      weight: 20
    - identifier: cloudsiem
      name: CloudSIEM
      parent: products
      url: /cloudsiem/
      weight: 30
    - identifier: iot
      name: IoT
      parent: products
      url: /iot/
      weight: 40
    # - identifier: content
    #   name: Content
    #   weight: 40
    - name: Community
      weight: 90
      url: https://community.netwitness.com
      post: <sup><i class="pl-1 fa-solid fa-up-right-from-square fa-xs"
        aria-hidden="true"></i></sup>

```

## Content Folder Structure

There seemed to be some initial confusion in how to structure the content folders.  This structure has a LOT of impact to how the navigation is managed.  I think this stemmed from a misunderstanding of how to make the MAIN navigation behave the way we intended.  As I mentioned above, the MAIN navigation bar can be manipulated independent of the actual content directory of the site as long as the `menu.main` properties are used (preferably only within the `config.yaml`)

Hugo has a strict set of rules for how content must be [organized](https://gohugo.io/content-management/organization/) and how this structure is made up of [page bundles](https://gohugo.io/content-management/page-bundles/).

> It is vitally important that anyone who is working on a Hugo site must understand the nuances of how content organization and the directory structure function.

Basically, each folder must have an index file (either `.md` or `.html`).  This page is going to be the default landing page when you navigate to its parent subfolder (i.e. `/mysection/`).  If the page is ONLY going to have "sibling" pages, then you would use the format `index.md`.  If the page is going to have sibling pages AND/OR "child" pages then you would use the format `_index.md`.

An index page with `_index.md` format (known as a "branch" page) can have a mix of files and subfolders as its children.  If a subfolder is created, then that subfolder must have either an `index.html` (no children) or `_index.md` (with children) as its landing page.

### About Page "Kinds"

Within a particular `type` or `layout` of a page, Hugo also recognizes different page `kind`.  Most pages are either a `list` or a `single` kind.  There are different templates that apply to these pages depending on this distinction.  Pages with a filename of `index.md` or `_index.md` are by default classified as `list`.  This will display a list of child pages in the body of the page.  Normally you don't want too much content on these pages to be defined in the `index.md`.  These pages are a jumping place to the child content that is underneath.

### Best Practice Suggestion for Directory Organization

We can create Main Menu item groupings without putting all the content under a single root folder (such as the case for the `Products` content).  This will add extra, vertical structure to the site map and will add extra items to the breadcrumbs when they don't need to be there.  It also takes up extra real-estate in the site navigation tree.  This subsequently makes the site more difficult to configure for navigation to avoid these confusing presentations.  Instead, we can put each product in its own root folder and just group them together under `Products` in the main menu.

The `_index.md` file (with children) should have minimal content in it.  The page will mainly be showing off its list of children.  An "Overview" page can be included (with `weight:1` to make it first) to guide the new user to click on it first.  If this is NOT the desired behavior then it can be overridden with front-matter settings.  Conversely, for `index.md` (no children) pages, we should treat this page as the default "overview" page, with all the content we want to display as such.

## Other ideas to customize behavior

Limit the depth of the left-tree TOC for topics that we don't want to display in the TOC but we want a list page to show them all.

- For instance with an FAQ or a HowTo list with long titles
- Should be doable with built-in Hugo properties

Hide the left-tree TOC altogether for pages like the MITRE matrix (to provide more horizontal real estate)

- Configurable in Hugo

Add the new "tags cloud" feature in the latest Docsy templates to enable users to search/navigate by tags
